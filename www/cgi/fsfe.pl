#!/usr/bin/perl

# SPDX-FileCopyrightText: 2020-2022 Luca Bonissi <lucabon@fsfe.org>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

$VERSION=164; # Try to keep the same version as git pull request

#use utf8;

BEGIN {
  $script = $ENV{SCRIPT_FILENAME};
  my $si=rindex($script,"/");
  if($si>=0) {
    $base=substr($script,0,$si);
    $script=substr($script,$si+1);
  }
  else {
    $base="./";
  }
  push(@INC,$base);
}

#binmode(STDOUT,":utf8");

use Symbol 'gensym';
use IPC::Open3;
use DBI;
use Time::HiRes qw(sleep);
use Encode qw(encode decode);
#use Diff qw(LCSidx); # included directly into this script

chdir($base); # Be ModPerl compliant

do("home.pm"); # Load the home page and other specific configurations

$gitbase="https://git.fsfe.org";
$gitbaseweb="https://git.fsfe.org/FSFE/fsfe-website";
$fsfebase="https://fsfe.org";

my $dbname="db/pad.db";

my $debug=0; # To debug regeneration of x[ht]ml

# Tags definition
my $tags_main = "blockquote|h[123456]|p|figcaption|li|dd|dt|a|fsfe-cd-donate-link";
my $tags_xml_main = "title|name|description|text";
my $tags_diff = "$tags_main|img|tag|chapter|subtitle";
my $tags_close = "$tags_diff|description|title|translator|sidebar|version|peertube|video";
my $tags_git_friendly = "blockquote|p|figcaption|li|dd|dt";

#### Diff.pm ###
# McIlroy-Hunt diff algorithm
# Adapted from the Smalltalk code of Mario I. Wolczko, <mario@wolczko.com>
# by Ned Konz, perl@bike-nomad.com
# Updates by Tye McQueen, http://perlmonks.org/?node=tye

# Create a hash that maps each element of $aCollection to the set of
# positions it occupies in $aCollection, restricted to the elements
# within the range of indexes specified by $start and $end.
# The fourth parameter is a subroutine reference that will be called to
# generate a string to use as a key.
# Additional parameters, if any, will be passed to this subroutine.
#
# my $hashRef = _withPositionsOfInInterval( \@array, $start, $end, $keyGen );

sub _withPositionsOfInInterval
{
    my $aCollection = shift;    # array ref
    my $start       = shift;
    my $end         = shift;
    my $keyGen      = shift;
    my %d;
    my $index;
    for ( $index = $start ; $index <= $end ; $index++ )
    {
        my $element = $aCollection->[$index];
        my $key = $keyGen ? &$keyGen( $element, @_ ) : $element;
        if ( exists( $d{$key} ) )
        {
            unshift ( @{ $d{$key} }, $index );
        }
        else
        {
            $d{$key} = [$index];
        }
    }
    return wantarray ? %d : \%d;
}

# Find the place at which aValue would normally be inserted into the
# array. If that place is already occupied by aValue, do nothing, and
# return undef. If the place does not exist (i.e., it is off the end of
# the array), add it to the end, otherwise replace the element at that
# point with aValue.  It is assumed that the array's values are numeric.
# This is where the bulk (75%) of the time is spent in this module, so
# try to make it fast!

sub _replaceNextLargerWith
{
    my ( $array, $aValue, $high ) = @_;
    $high ||= $#$array;

    # off the end?
    if ( $high == -1 || $aValue > $array->[-1] )
    {
        push ( @$array, $aValue );
        return $high + 1;
    }

    # binary search for insertion point...
    my $low = 0;
    my $index;
    my $found;
    while ( $low <= $high )
    {
        #$index = ( $high + $low ) / 2;       # with 'use integer'
        $index = int(( $high + $low ) / 2);  # without 'use integer'

        $found = $array->[$index];

        if ( $aValue == $found )
        {
            return undef;
        }
        elsif ( $aValue > $found )
        {
            $low = $index + 1;
        }
        else
        {
            $high = $index - 1;
        }
    }

    # now insertion point is in $low.
    $array->[$low] = $aValue;    # overwrite next larger
    return $low;
}

# This method computes the longest common subsequence in $a and $b.

# Result is array or ref, whose contents is such that
#   $a->[ $i ] == $b->[ $result[ $i ] ]
# foreach $i in ( 0 .. $#result ) if $result[ $i ] is defined.

# An additional argument may be passed; this is a hash or key generating
# function that should return a string that uniquely identifies the given
# element.  It should be the case that if the key is the same, the elements
# will compare the same. If this parameter is undef or missing, the key
# will be the element as a string.

# By default, comparisons will use "eq" and elements will be turned into keys
# using the default stringizing operator '""'.

# Additional parameters, if any, will be passed to the key generation
# routine.

sub _longestCommonSubsequence
{
    my $a        = shift;    # array ref or hash ref
    my $b        = shift;    # array ref or hash ref
    my $counting = shift;    # scalar
    my $keyGen   = shift;    # code ref
    my $compare;             # code ref

    if ( ref($a) eq 'HASH' )
    {                        # prepared hash must be in $b
        my $tmp = $b;
        $b = $a;
        $a = $tmp;
    }

    # Check for bogus (non-ref) argument values
    if ( !ref($a) || !ref($b) )
    {
        my @callerInfo = caller(1);
        die 'error: must pass array or hash references to ' . $callerInfo[3];
    }

    # set up code refs
    # Note that these are optimized.
    if ( $keyGen )    # optimize for strings
    {
        $compare = sub {
            my $a = shift;
            my $b = shift;
            &$keyGen( $a, @_ ) eq &$keyGen( $b, @_ );
        };
    }

    my ( $aStart, $aFinish, $matchVector ) = ( 0, $#$a, [] );
    my ( $prunedCount, $bMatches ) = ( 0, {} );

    if ( ref($b) eq 'HASH' )    # was $bMatches prepared for us?
    {
        $bMatches = $b;
    }
    else
    {
        my ( $bStart, $bFinish ) = ( 0, $#$b );

        # First we prune off any common elements at the beginning
        while ( $aStart <= $aFinish
            and $bStart <= $bFinish
            and ( $keyGen ? &$compare( $a->[$aStart], $b->[$bStart], @_ )
                          : ( $a->[$aStart] eq $b->[$bStart] ) ) )
        {
            $matchVector->[ $aStart++ ] = $bStart++;
            $prunedCount++;
        }

        # now the end
        while ( $aStart <= $aFinish
            and $bStart <= $bFinish
            and ( $keyGen ? &$compare( $a->[$aFinish], $b->[$bFinish], @_ )
                          : ( $a->[$aFinish] eq $b->[$bFinish] ) ) )
        {
            $matchVector->[ $aFinish-- ] = $bFinish--;
            $prunedCount++;
        }

        # Now compute the equivalence classes of positions of elements
        $bMatches =
          _withPositionsOfInInterval( $b, $bStart, $bFinish, $keyGen, @_ );
    }
    my $thresh = [];
    my $links  = [];

    my ( $i, $ai, $j, $k );
    for ( $i = $aStart ; $i <= $aFinish ; $i++ )
    {
        $ai = $keyGen ? &$keyGen( $a->[$i], @_ ) : $a->[$i];
        if ( exists( $bMatches->{$ai} ) )
        {
            $k = 0;
            for $j ( @{ $bMatches->{$ai} } )
            {

                # optimization: most of the time this will be true
                if ( $k and $thresh->[$k] > $j and $thresh->[ $k - 1 ] < $j )
                {
                    $thresh->[$k] = $j;
                }
                else
                {
                    $k = _replaceNextLargerWith( $thresh, $j, $k );
                }

                # oddly, it's faster to always test this (CPU cache?).
                if ( defined($k) )
                {
                    $links->[$k] =
                      [ ( $k ? $links->[ $k - 1 ] : undef ), $i, $j ];
                }
            }
        }
    }

    if (@$thresh)
    {
        return $prunedCount + @$thresh if $counting;
        for ( my $link = $links->[$#$thresh] ; $link ; $link = $link->[0] )
        {
            $matchVector->[ $link->[1] ] = $link->[2];
        }
    }
    elsif ($counting)
    {
        return $prunedCount;
    }

    return wantarray ? @$matchVector : $matchVector;
}

sub LCSidx
{
    my $a= shift @_;
    my $b= shift @_;
    my $match= _longestCommonSubsequence( $a, $b, 0, @_ );
    my @am= grep defined $match->[$_], 0..$#$match;
    my @bm= @{$match}[@am];
    return \@am, \@bm;
}
###############



##############  GENERAL UTILITIES ##################
sub ReadParam {
  my $options=shift;
  my $readin;
  if($ENV{REQUEST_METHOD} eq "POST")
  {
    read(STDIN,$readin,$ENV{'CONTENT_LENGTH'});
  }
  else
  {
    if(($options & 1)==0) { $readin=$ENV{QUERY_STRING}; }
  }
  return $readin;
}

sub ReadParse {
   my $in = shift;
   my $divisor = shift;
   my $conv = shift;

   if(!defined($conv)) {$conv=1;}

   my ($i, $loc, $key, $val, %in, @in, $duplicate);
   undef $duplicate;

   @in = split(/$divisor/,$in);

   foreach $i (0 .. $#in) {
     # Convert pluses to spaces
     if($conv) {$in[$i] =~ s/\+/ /g;}

     # Split into key and value.
     ($key, $val) = split(/=/,$in[$i],2); # splits on the first =.

     # Convert %XX from HEX numbers to alphanumeric
     # but does not handle UTF-16 escape %uXXXX
     if($conv) {
       $key =~ s/%(..)/pack("c",hex($1))/ge;
       $val =~ s/%(..)/pack("c",hex($1))/ge;
     }

     # Associate key and value
     #$in{$key} .= "\0" if (defined($in{$key})); # \0 is the multiple separator
     #$in{$key} .= "+" if (defined($in{$key})); # + is the multiple separator
     #$in{$key} .= $val;
     $in{$key} = $val;

   }
   return %in;
}

sub SplitCookie {
   my($cookie,$divisor,$paramsep)=@_;
   my(@c,%c,$i,$key,$val);
   if($divisor eq "") { $divisor=" "; }
   if($paramsep eq "") { $paramsep=":"; }
   @c = split(/$divisor/,$cookie);
   foreach $i (0 .. $#c) {
     # Split into key and value.
     ($key, $val) = split(/$paramsep/,$c[$i],2); # splits on the first $paramsep
     $c{$key}=$val;
   }
   return(%c);
}

sub SplitExtra {
  return(SplitCookie($_[0],"\t","="));
}

sub JSQuote {
  my $str = shift;
  my ($s1,$s2);
  $str =~ s/\\/\\\\/gs;
  $str =~ s/"/\\"/gs;
  $str =~ s/\r\n/\n/gs;
  $str =~ s/\n/\\n/gs;
  return $str;
}

sub ValQuote {
  my $str = shift;
  my ($s1,$s2);
  $s1="\"";$s2="&quot;";
  $str =~ s/$s1/$s2/ges;
  return $str;
}

sub HTMLQuote {
  my $str = shift;
  my ($s1,$s2);
  $s1="&";$s2="&amp;";
  $str =~ s/$s1/$s2/ges;
  $s1="\"";$s2="&quot;";
  $str =~ s/$s1/$s2/ges;
  $s1="<";$s2="&lt;";
  $str =~ s/$s1/$s2/ges;
  $s1=">";$s2="&gt;";
  $str =~ s/$s1/$s2/ges;
  $str =~ s/\r\n/\n/g;
  return $str;
}
sub HTMLUnQuote {
  my $str = shift;
  my ($s1,$s2);
  $s2="\"";$s1="&quot;";
  $str =~ s/$s1/$s2/ges;
  $s2="<";$s1="&lt;";
  $str =~ s/$s1/$s2/ges;
  $s2=">";$s1="&gt;";
  $str =~ s/$s1/$s2/ges;
  $s2="&";$s1="&amp;";
  $str =~ s/$s1/$s2/ges;
  $str =~ s/\r\n/\n/g;
  return $str;
}
sub TitleQuote {
  my $str = shift;
  my ($s1,$s2);
  $s1="&";$s2="&amp;";
  $str =~ s/$s1/$s2/ges;
  $s1="<";$s2="&lt;";
  $str =~ s/$s1/$s2/ges;
  $s1=">";$s2="&gt;";
  $str =~ s/$s1/$s2/ges;
  return $str;
}

sub HTMLQuoteNBSP {
  my $str = HTMLQuote(shift);
  if(length($str)==0) { $str="&nbsp;"; }
  return $str;
}

sub HTMLQuoteBR {
  my $str = HTMLQuoteNBSP(shift);
  $str =~ s/\n/<BR>\n/g;
  return($str);
}

sub GetHTTP {
  my $url = $_[0];
  my ($stdout,$stderr);
  eval {
    local $SIG{ALRM} = sub { die "alarm\n" };
    my($wtr, $rdr, $err);
    $wtr=gensym;
    $rdr=gensym;
    $err=gensym;
    $pid = open3($wtr,$rdr,$err, "wget -O - \"$url\"");
    alarm(30);
    close($wtr);
    while(<$rdr>) { $stdout.=$_; }
    close($rdr);
    while(<$err>) { $stderr.=$_; }
    waitpid( $pid, 0 );
    alarm(0);
    close($err);
  };
  if(exists($_[1])) { $_[1]=$stderr; }
  return($stdout);
}

##############  DB FUNCTIONS ##################
my $db=undef;
my $mapref;

sub LowerCursorNames {
  my $cur=shift;
  my $numfields=$cur->{NUM_OF_FIELDS};
  for(my $i=0;$i<$numfields;$i++) { $cur->{NAME}[$i] = lc($cur->{NAME}[$i]); }
}

sub sqlexe {
  my ($db,$sql,@param)=@_;
  #if(!$db) { return(undef); }
  my $cursor=$db->prepare($sql);
  if(defined($cursor) && !$db->err) {
    $execresult=$cursor->execute(@param);
    if(!$db->err) {
      LowerCursorNames($cursor);
      undef(%mapf);
      $mapref=\%mapf;
      return($cursor);
    }
    else {
      print STDERR scalar(localtime)." - Error executing SQL \"$sql\" (".join(",",@param)."): ".$db->errstr."\n";
      return(undef);
    }
  }
  else {
    print STDERR scalar(localtime)." - Error preparing SQL \"$sql\": ".$db->errstr."\n";
    return(undef);
  }
}

sub sqldo {
  my $cursor=sqlexe(@_);
  $cursor->finish if(defined($cursor));
  return $execresult;
}

sub mapfield {
  my ($cur,$name,$varref,$mapref_loc)=@_;
  my ($col,$tmpname,$i,$dot);
  if(!defined($mapref_loc)) { $mapref_loc=$mapref; }
  if(!defined($mapref_loc->{chr(9)}))
  {
    my $numfields=$cur->{NUM_OF_FIELDS};
    for(my $i=0;$i<$numfields;$i++) {
      $tmpname=$cur->{NAME}[$i];
      if(!defined($mapref_loc->{$tmpname})) { $mapref_loc->{$tmpname} = $i+1; }
      if(($dot=rindex($tmpname,"."))>=0)
      {
	$tmpname = substr($tmpname,$dot+1);
        if(!defined($mapref_loc->{$tmpname})) { $mapref_loc->{$tmpname} = $i+1; }
      }
    }
    $mapref_loc->{chr(9)}=1;
  }
  if(defined($col=$mapref_loc->{$name}))
  {
    $cur->bind_col($col,$varref);
    return($col);
  }
  else
  {
    print STDERR "ERROR: No field '$name'\n";
  }
  return(undef);
}

sub AddWhere {
  my($where,$ar,$sql,@param)=@_;
  if(length($$where)>0) {
    $$where .= " AND ($sql)";
  }
  else {
    $$where = "WHERE ($sql)";
  }
  push @{$ar},@param;
}

sub FixJolly {
  my($val)=@_;
  $val=~s/\*/%/g;
  $val=~s/\?/_/g;
  return($val);
}


sub ConnectDB {
  my $tmp_db_file=shift;
  my ($dbtmp,$tries);
  while(!$dbtmp && $tries<3)
  {
    eval { $dbtmp=DBI->connect("dbi:SQLite:dbname=$tmp_db_file","","",{PrintError=>1}); };
    #eval { $dbtmp=DBI->connect("dbi:SQLite:dbname=$tmp_db_file","","",{PrintError=>1, sqlite_unicode=>1}); };
    if(!$dbtmp) { sleep 1; }
    $tries++;
  }
  if(!$dbtmp) {
    print STDERR "Cannot open database: $DBI::err_str (".$DBI::err.")\n";
  }
  return $dbtmp;
}

sub CloseDB {
  $_[0]->disconnect if($_[0]);
  undef $_[0];
}


##############  CUSTOM FUNCIONS ##################
$debugopen=0;

sub Debug {
  my $ver=shift;
  if($debug>=$ver) {
    if($debugopen<=0) {
      open(DBG,($debugopen<0?">":"").">buf.txt");
      my $old=select(DBG);
      $|=1;
      select($old);
      $debugopen=1;
    }
    print DBG @_;
  }
}

sub DebugClose {
  if($debugopen) { 
    close(DBG);
    $debugopen=-1;
  }
}

sub AddCss
{
  my($s);
  $s="\n  <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\"/>\n  <meta customversion=\"$VERSION\"/>\n  <link rel=\"stylesheet\" href=\"$fsfehost/look/fsfe.min.css\" type=\"text/css\"/>\n";
  if(-f "$ENV{DOCUMENT_ROOT}$homedir/custom.css")
  {
    $s.="\n  <link rel=\"stylesheet\" href=\"$homedir/custom.css\" type=\"text/css\"/>";
  }
  $s.="\n<style>\n".
      "<!--\n".
       "ins { color: darkgreen; text-decoration: none; background-color: #D0FFD0; }\n".
       "del { color: red; }\n".
      ".imgalt { font-style: italic; font-size: 0.7em; }\n".
      "button, .editbtn { box-sizing: border-box; -webkit-appearance: button; background-color: #3394ce; color: #fff; border-radius: 4px; line-height: 1.42857143; padding: 6px 12px; white-space: nowrap; vertical-align: middle; display: inline-block; margin-bottom: 0; border: 1px solid #2d86bb; }\n".
      "button:hover, .editbtn:hover { border-color: #216289; background-color: #2877a6 }\n".
      ".btngreen, .btngreen:focus { border-color: #277923; background-color: #17ab11; }\n".
      ".btngreen:hover { border-color: #1f601b; background-color: #279323 }\n".
      ".btngray, .btngray:focus { border-color: #606060; background-color: #808080; }\n".
      ".btngray:hover { border-color: #404040; background-color: #606060 }\n".
      ".btnred, .btnred:focus { border-color: #DF0000; background-color: #FF0000; }\n".
      ".btnred:hover { border-color: #BF0000; background-color: #DF0000 }\n".
      ".btnyellow, .btnyellow:focus { border-color: #DFDF00; background-color: #FFFF00; color: black; }\n".
      ".btnyellow:hover { border-color: #BFBF00; background-color: #DFDF00 }\n".
      ".btnlgreen, .btnlgreen:focus { border-color: #60DF60; background-color: #80FF80; color: black; }\n".
      ".btnlgreen:hover { border-color: #40BF40; background-color: #60DF60 }\n".
      "-->\n".
      "</style>\n";
  return($s);
}

$lastspaces="";

sub GetNextTag {
  my($html,$i)=@_;
  my($i2,$i3,$i4,$i5,$tag,$isquote,$ch);
  if(!defined($i)) { $i2=-2; }
  else { $i2=index($$html,"<",$i); }
  if($i2>=0) {
    $isquote=1;
    $i4=$i2;
    $i3=$i2;
    while($isquote) {
      # Check if the ">" is inside quote
      $i3=index($$html,">",$i3);
      $i4=index($$html,"\"",$i4);
      if($i4<0 || $i4>$i3) {
        $isquote=0;
      }
      else {
        # Get closing quote
	$i4++;
        $i4=index($$html,"\"",$i4);
	if($i4>$i3) { $i3++; }
	if($i4<0) { $isquote=0; }
	$i4++;
      }
      #if(substr($$html,$i2+1,3) eq "img") {
      #  print STDERR "I2=$i2 I3=$i3 I4=$i4 $isquote\n";
      #}
    }
    $i5=$i3;

    $closetag="/>";
    ($tag)=(substr($$html,$i2)=~m|<(/?\S+)|si);
    $i4=index($tag,$closetag);
    if($i4>0) { $tag=substr($tag,0,$i4); }
    $i4=index($tag,">");
    if($i4>0) { $tag=substr($tag,0,$i4); }
    $tag=~s/\s+//g;

    $i3=$i5;
    $i4=0;
    if($tag eq "!--") {
      if(substr($$html,$i3-2,2) eq "--")
      {
        $i4=1;
      }
    }
    if(substr($$html,$i3-length($closetag)+1,length($closetag)) eq $closetag) { $i4=1; }
    if($i4>0 || ($tag eq "img" && $isdiff)) {
      # Self-closing tag
      $i3++;
      if(substr($$html,$i3,1) eq "\n") { $i3++; }
      $i4=1;
    }
    else {
      $i3++;
      if(substr($tag,0,1) eq "/") { $i4=2; }
      else { $i4=0; }
    }
    Debug(1,($i4==1?"Close":($i4==2?"Open/Close":"Open"))." $tag => $i2 - $i3 - $i4\n");
    $lastspaces="";
    $i=$i2-1;
    while($i>=0) {
      $ch=substr($$html,$i,1);
      if($ch=~m/[ \t]/) { $lastspaces="$ch$lastspaces"; $i--; }
      else { last; }
    }
  }
  return(($tag,$i2,$i3,$i4));
}

sub GetCloseTag {
  my($html,$tag,$i)=@_;
  my($i2,$i3,$closetag,$opentag);
  while($i>=0) {
    ($closetag)=(substr($$html,$i)=~m|(</$tag\s*>)|si);
    if(length($closetag)>0) {
      $i2=index($$html,$closetag,$i);
      Debug(5,"Closetag=$closetag ($i2)\n");
      # check if we have other[s] open tags before the close tag
      ($opentag)=(substr($$html,$i)=~m|(<$tag)|si);
      if(length($opentag)>0) {
        $i=index($$html,$opentag,$i);
      }
      else {
        $i=-1;
      }
      if($i<$i2 && $i>=0) {
        $i++;
        ($closetag)=(substr($$html,$i)=~m|(</$tag\s*>)|si);
	if(index($$html,$closetag,$i)==$i2) { # Should be always true??
	  $i=$i2+1;
	}
      }
      else {
        $i=-1;
	$i3=$i2+length($closetag);
	if(substr($$html,$i3,1) eq "\n") { $i3++; }
      }
    }
    else {
      $i=-1;
    }
  }
  Debug(4,"Closetag $tag - $i2 - $i3\n");
  return(($i2,$i3));
}

sub GetCloseTagContent {
  my($html,$tag,$i,$casehtml)=@_;
  my($ic2,$ic3,$inner,$ctag);
  if(!defined($casehtml)) { $casehtml=$html; }
  ($ic2,$ic3)=GetCloseTag($html,$tag,$i);
  if($ic2>=0) {
    $inner=substr($$casehtml,$i,$ic2-$i);
    $ctag=substr($$casehtml,$ic2,$ic3-$ic2);
  }
  else {
    $inner=$ctag="";
    $ic3=$i;
  }
  return($ic2,$ic3,$inner,$ctag);
}

sub CheckASplit {
  my($html,$newhtml)=@_;
  my($i,$i2,$k,$k2,$k3,$j,$spc,$ospc);
  while($i>=0 && $k>=0) {
    $ospc="";
    $i2=index($html,"<a ",$i);
    $i3=index($html,"<a\n",$i);
    if($i3>=0 && $i3<$i2) {
      $i=$i3;
      $i2=1;
      $ospc="";
    }
    else {
      $i=$i2;
      $ospc=substr($html,$i+2);
      if($ospc=~m/^\s+\n/s) { $i2=1; $ospc=substr($ospc,0,index($ospc,"\n")); }
      else { $i2=0; $ospc=" "; }
    }
    $k2=index($newhtml,"<a ",$k);
    $k3=index($newhtml,"<a\n",$k);
    if($k3>=0 && $k3<$k2) {
      $k=$k3;
      $k2=1;
    }
    else {
      $k=$k2;
      if(substr($newhtml,$k+2)=~m/^\s+\n/s) { $k2=1; }
      else { $k2=0; }
    }
    if($i>=0 && $k>=0) {
      # Check if it is to be splitted
      if($i2) {
        # $newhtml has to be split
        if(!$k2) {
	  # Get preeceding space:
	  $j=rindex($html,"\n",$i);
	  if($j>=0) {
	    $_=substr($html,$j+1,$i-$j);
	    ($spc)=m/^(\s+)/;
	  }
	  else {
	    $spc="";
	  }
          $k+=2;
          $newhtml=substr($newhtml,0,$k).$ospc."\n".$spc.substr($newhtml,$k+1);
	  $k+=length($spc)+length($ospc)+1;
        }
      }
      $i++;
      $k++;
    }
  }
  return($newhtml);
}

$linebreak=72;
$linebreakmax=$linebreak+5;
$aactive=0;

sub SplitLine {
  my($s,$lb,$ls,$addls)=@_;
  my($o,$i,$i2);
  if($aactive) { $aactive=2; }
  while(length($s)>$lb) {
    if($aactive) {
      if(($i2=index($s,">"))>=0) {
	$aactive=0;
	$o.=substr($s,0,$i2+1);
	$s=substr($s,$i2+1);
        $i=rindex($o,"\n");
	if($i<0) { $i=0; }
	$i=length($o)-$i;
	if($i>$lb) {
	  if(substr($s,0,1) eq " ") {
	    $o.=" ";
	    $s=substr($s,1);
	  }
          if(length($s)>0) { $o.="\n"; }
          $addls=$ls;
	  next;
	}
	elsif(($i+length($s))>$lb) {
	  # Should reduced the total length considering bytes already filled
	  # with the unbreackable tag
	  $i2=$lb-$i;
          $i=rindex($s," ",$i2);
          if($i==$lb && length($s)==($i2+1)) { last; }
          if($i<=0) {
            $i=index($s," ",$i2);
          }
	  $i++;
          $o.=substr($s,0,$i);
          $s=substr($s,$i);
          if(length($s)>0) { $o.="\n"; }
          $addls=$ls;
	  next;
	}
      }
      else {
        last;
      }
    }
    $i=rindex($s," ",$lb);
    if($i==$lb && length($s)==($lb+1)) { last; }
    if($i<=0) {
      $i=index($s," ",$lb);
    }
    if($i<0) { last; }
    $i2=index(lc(substr($s,0,$i+3)),"<a");
    if($i2>$i) {
      # Check if it is really an anchor
      if($i2==(length($s)-2)) {
        $aactive=1;
	$i+=2;
      }
      elsif(substr($s,$i2+2,1) eq " ") {
        $aactive=1;
	$i+=3;
      }
    }
    else {
      # Check for IMG tag
      $i2=index(lc(substr($s,0,$i)),"<img");
      if($i2>=0) {
        $aactive=1;
	$i+=4;
      }
    }
    $i++;
    if($aactive<=1) { $o.=$addls; }
    $o.=substr($s,0,$i);
    $addls=$ls;
    $s=substr($s,$i);
    if(length($s)>0 && !$aactive) { $o.="\n"; }
  }
  if(length($s)>0 && !$aactive) {
    $o.=$addls;
  }
  $o.=$s;
  return($o);
}

sub LineBreakLen {
  my($ls)=@_;
  my($lb);
  $lb=$linebreak-length($ls);
  if($lb<40) { $lb=40; }
  return($lb);
}

sub JoinLines
{
  @lines=@_;
  my($s);
  $s="";
  for($i=0;$i<=$#lines;$i++)
  {
    $lines[$i]=~s/^\s+|\s+$//g;
    if(length($lines[$i])>0) {
      $s.=$lines[$i]." ";
    }
  }
  chop($s);
  return($s);
}

sub GitFriendly {
  my($s,$os,$lastcr)=@_;
  my($lb,@lines,$olines,$max,$min,$ave,$i,$l,$ls,$ofirst,$olast);
  if($formatting{linebreak} eq "none" || (!$exporting && !$formatting{savesame}) ) {
    return($s);
  }
  $s=decode('utf8',$s);
  $ls=$lastspaces;
  $lb=LineBreakLen($ls);
  @lines=split("\n",$s,-1);
  if($formatting{linebreak} eq "join") {
    @lines=(JoinLines(@lines));
  }
  else {
    $ave=$lb*1.5;
    @olines=split("\n",$os,-1);
    if($#olines==0 && $#lines==0) {
      # Check if we have to leave integral (e.g. <a href=....>text</a> or <img....../>):
      if($olines[0]=~m/^\s*</ && $olines[0]=~m/>\s*$/) {
        return(encode('utf8',$s));
      }
    }
    if(length($lines[0])>0 && !($lines[0]=~m/^\s*$/)) {
      # Check if we have to join lines
      $max=0;
      for($i=0;$i<=$#lines;$i++)
      {
	if(length($lines[$i])>$ave) { $max++; }
      }
      if($max>=(($#lines+1)/2)) {
        @lines=(JoinLines(@lines));
      }
    }
    $max=0;
    $min=-1;
    #$ave=0;
    for($i=0;$i<=$#lines;$i++)
    {
      $l=length($lines[$i]);
      if($l>0) {
	if($l>$max) { $max=$l; }
	if($min<0 || $l<$min) { $min=$l; }
	#$ave+=$l;
      }
    }
    #if($ave>0) { $ave=$ave/($#lines+1); }
    if($olines[0]=~m/^\s*$/) { $ofirst=-1; }
    elsif($#olines==0) { $ofirst=1; }
    else { $ofirst=2; }
    if($olines[$#olines]=~m/^\s*$/) { $olast=-1; }
    else { $olast=1; }

    # Check if lines are to be splitted
    $aactive=0;
    for($i=0;$i<=$#lines;$i++)
    {
      if($i==0 && $lines[$i]=~m/^\s*$/) { $ofirst=0; next; }
      $l=length($lines[$i]);
      if($l>0) {
	if($lines[$i]=~m/^\s/ && ($i>0 || $lastcr)) {
	  # Update last spaces
	  ($ls)=($lines[$i]=~m/(^\s+)/);
	  $lb=LineBreakLen($ls);
	  $lines[$i]=~s/^\s+//;
	  $l=length($lines[$i]);
	}
	if($l>$ave) {
	  $lines[$i]=SplitLine($lines[$i],$lb,$ls,($ofirst>=2?"":$ls));
	  if($ofirst==1) {
	    $olast=-2;
	    $lines[$i]="\n".$lines[$i];
	  }
	}
	else {
	  $lines[$i]=($ofirst>=1?"":$ls).$lines[$i];
	}
	if($ofirst<0) {
	  $lines[$i]="\n".$lines[$i];
	}
	if($i==$#lines) {
	  if(!($lines[$i]=~m/^\s*$/) && ($olast<0)) {
	    $lines[$i].="\n$lastspaces";
	  }
	}
      }
      $ofirst=0;
    }
  }
  return(encode('utf8',join("\n",@lines)));
}

sub SplitSections
{
  my $html=shift;
  my ($htmllc);
  my (@sec,$s);
  my ($i,$i2,$i3,$tag,$tagclosed,$ic2,$ic3);
  my ($xml);
  $i=0;
  $s=0;
  $htmllc=lc($html);
  while(defined($i) && $i2>=0) {
    ($tag,$i2,$i3,$tagclosed)=GetNextTag(\$htmllc,$i);
    if($i2<0) {
      $sec[$s]{tag}="";
      $sec[$s]{buf}=substr($html,$i);
      $sec[$s]{nodiff}=1;
      $s++;
      last;
    }
    $_=substr($html,$i2,$i3-$i2);
    $sec[$s]{lastspaces}=$lastspaces;
    $sec[$s]{tag}=$tag;
    $sec[$s]{pre}=substr($html,$i,$i2-$i);
    $sec[$s]{tagfull}=$_;
    $sec[$s]{tagclosed}=$tagclosed;
    $i=$i3;

    if($tagclosed==0 &&
       ($tag=~m/^($tags_close)$/ ||
        ($xml && ($tag =~m/^($tags_xml_main)$/))
       )
      )
    {
      ($ic2,$ic3)=GetCloseTag(\$htmllc,$tag,$i3);
      if($ic3>0) {
	$i=$ic3;
	$sec[$s]{buf}=substr($html,$i3,$ic2-$i3);
	$sec[$s]{ctag}=substr($html,$ic2,$ic3-$ic2);
        if(!($tag=~m/^($tags_diff)/)) {
	  $sec[$s]{nodiff}=1;
	}
      }
    }
    if($tag=~m|\S+set$| || $tag eq "feed" || $tag eq "data") {
      # pure XML file
      $xml=1;
    }
    $s++;
  }
  return(@sec);
}

sub GetSecTags
{
  my(@sec)=@_;
  my ($i,@t);
  for($i=0;$i<=$#sec;$i++)
  {
    if(length($sec[$i]{tag})>0) {
      push(@t,$sec[$i]{tag});
    }
  }
  return(@t);
}

sub CleanImg
{
  my ($s,$so)=@_;
  my($i,$i2,$i3,$tag,$tagclosed,$sl,$s2,$sol,$img,$alt,$src,$ls,$origalt,$imgo);
  my($o,$otag,$o2,$o3,$ospc);
  # Clean added IMG fields
  $i=0;
  $o=0;
  $sl=lc($s);
  $sol=lc($so);
  $s2=$s;
  $s="";
  $ls=$lastspaces;
  while($i>=0) {
    if(($i=index($sl,"<img",$i))>=0) {
      ($tag,$i2,$i3,$tagclosed)=GetNextTag(\$sl,$i);
      if($i2>=0) {
	$origalt=-1;
	$imgo=undef;
        if(($o=index($sol,"<img",$o))>=0) {
          ($otag,$o2,$o3,$tagclosed)=GetNextTag(\$sol,$o);
	  if($o2>=0) {
	    $imgo=substr($so,$o2,$o3-$o2);
	    #($lastimgspc)=($imgo=~m|(\s*)/>$|);
	    $so=substr($so,$o3);
	    $sol=substr($sol,$o3);
	  }
	  else {
	    $so="";
	  }
	}
        $s.=substr($s2,0,$i2);
	$img=substr($s2,$i2,$i3-$i2);
	$s2=substr($s2,$i3);
	$sl=substr($sl,$i3);
	$img=~s/ title=".*?"//gis;
	$img=~s/ onclick=".*?"//gis;
	$img=~s/ id="webpreview.*?"//gis;
	#$img=~s/oncontextmenu=".*?" //gis;
	if(length($imgo)==0) {
	  $imgo=$img; # Should not happen, but to avoid loss of data (e.g. IMG copied)
	}
        if(lc($imgo)=~m/alt=\s*"/si) { $origalt=1; }
        $alt=undef;
        ($src)=($img=~m|src=\s*"(.*?)"|si);
	$imgo=~s/(src=\s*)"(.*?)"/$1"$src"/si;
        if($img=~m|alt=\s*"|si) {
	  # Fix < and > to &lt; and &gt;
          ($alt)=($img=~m|alt=\s*"(.*?)"|si);
	  $alt=~s/</\&lt;/gs;
	  $alt=~s/>/\&gt;/gs;
	  if(length($alt)==0 && $origalt<0) {
	    $alt=undef;
	  }
	  elsif($origalt<0) {
	    $imgo=~s/<img/<img alt="$alt"/si;
	  }
	  else {
	    $imgo=~s/(alt=\s*)"(.*?)"/$1"$alt"/si;
	  }
        }
	elsif($origalt>=0) {
	  $imgo=~s/(alt=\s*)"(.*?)"/$1""/si;
	}
	$s.=$imgo;
	$i3=0;
      }
      else { $i3=-1; }
      $i=$i3;
    }
  }
  $s.=$s2;

  # Clean all other fields
  $i=0;
  $sl=lc($s);
  $s2=$s;
  $s="";
  while($i>=0) {
    if(($i=index($sl,"<",$i))>=0) {
      ($tag,$i2,$i3,$tagclosed)=GetNextTag(\$sl,$i);
      if($i2>=0) {
        $s.=substr($s2,0,$i2);
	$img=substr($s2,$i2,$i3-$i2);
	$s2=substr($s2,$i3);
	$sl=substr($sl,$i3);
	$img=~s/ id="teaser"//gis;
	$img=~s/ id="webpreview.*?"//gis;
	$img=~s/oncontextmenu=".*?" //gis;
	$img=~s/ onclick=".*?"//gis;
	$img=~s/ onfocus=".*?"//gis;
	$img=~s/ contenteditable=".*?"//gis;
	$s.=$img;
	$i3=0;
      }
      else { $i3=-1; }
      $i=$i3;
    }
  }
  $s.=$s2;
  $lastspaces=$ls;
  return($s);
}

sub RebuildBody
{
  my ($oldhtml,$newhtml)=@_;
  my ($i,$k,$j,$s);
  my (@so,@sn,@tago,@tagn,$idxo,$idxn,@to);
  my ($tag,$pre,$kfull,$ctag,$kinner,$inner,$buf,$ls,%skipnextclose,%tagopen);
  @so=SplitSections($oldhtml);
  @sn=SplitSections($newhtml);
  @tago=GetSecTags(@so);
  @tagn=GetSecTags(@sn);
  Debug(1,"TAGO=".join(" # ",@tago)."\n");
  Debug(1,"TAGN=".join(" # ",@tagn)."\n");
  ($idxo,$idxn)=LCSidx(\@tago,\@tagn);
  push(@{$idxo},$#tago+1); # To avoid repetition outside while
  push(@{$idxn},$#tagn+1); # To avoid repetition outside while

  # Compose the tag order based on newhtml
  $i=0;
  $j=0;
  $k=0;
  while($i<=$#$idxn) {
    while($i<=$#$idxn && $idxo->[$i]==$j && $idxn->[$i]==$k) {
      push(@to,"$j $k");
      $i++;
      $j++;
      $k++;
    }
    while($j<$idxo->[$i]) {
      push(@to,"$j -1");
      $j++;
    }
    while($k<$idxn->[$i]) {
      push(@to,"-1 $k");
      $k++;
    }
  }

  # Compose the body section by section
  $buf="";
  #$lastimgspc="";
  for($i=0;$i<=$#to;$i++)
  {
    ($j,$k)=split(" ",$to[$i]); # $j=old x[ht]ml tag index, $k=new x[ht]ml tag index
    if($j>=0) {
      $tag=$so[$j]{tag};
      $pre=$so[$j]{pre};
      $inner=$so[$j]{buf};
      $kfull=$so[$j]{tagfull};
      $ctag=$so[$j]{ctag};
      $ls=$so[$j]{lastspaces};
      if($k>=0) {
        $pre=$sn[$k]{pre};
	if($tag eq "a" || $tag eq "img") {
	  $kfull=CleanImg($sn[$k]{tagfull},$so[$j]{tagfull});
	}
	if(!$so[$j]{tagclosed}) {
	  $tagopen{"/$tag"}++;
	}
	elsif($so[$j]{tagclosed} && !$sn[$k]{tagclosed}) {
	  # The browser has splitted the self-closed tag into open/close tag:
	  # skip the next close tag since it is already closed here
	  if($kfull=~m/\n$/s) { chop($kfull); }
	  $skipnextclose{"/$tag"}++;
	}
        $kinner=$sn[$k]{buf};
      }
      elsif($tagopen{$tag}==0) {
        # Skip it
	next;
      }
      else {
        # Browser has removed the closed tag (e.g. <source....> for <video>)
        $tagopen{$tag}--;
        $pre=$so[$j]{pre};
        $kinner="";
      }
    }
    else {
      $tag=$sn[$k]{tag};
      if($tag eq "video" && $sn[$k]{tagfull}=~m/ peertube=/) {
        # The <video> tag was added by webpreview, rebuild peertube link
	($kfull)=($sn[$k]{tagfull}=~m/ peertube="(.*?)"/si);
	$kfull=HTMLUnQuote($kfull);
	$inner=$kinner=$ctag="";
      }
      else {
	$pre=$sn[$k]{pre};
	$ls=$sn[$k]{lastspaces};
	if($sn[$k]{tagclosed} && $skipnextclose{"$tag"}) {
	  $buf.=$pre;
	  $skipnextclose{"$tag"}--;
	  next;
	}
	$kfull=CleanImg($sn[$k]{tagfull});
	$inner="";
	$ctag=$sn[$k]{ctag};
	$kinner=$sn[$k]{buf};
      }
    }
    if($tag eq "version") { $kinner=$in{version}; }
    Debug(1,"PRE=${pre}## TAG=$tag ## BUF=${kinner}##\n");
    $buf.=$pre;
    Debug(1,"$i) $tag ($ctag) -- $kfull\n$kinner\n");
    if($xml || $tag=~m/^($tags_main)$/i)
    {
      $kinner=~s|<br/?>$||gis; # Remove any trailing <br>
      $kinner=CleanImg($kinner,$inner); # To allow <img> tag to be splitted as the original
      if($tag=~m/^($tags_git_friendly)/i)
      {
	$lastspaces=$ls;
        $kinner=GitFriendly($kinner,$inner,substr($kfull,-1) eq "\n");
      }
      elsif($xml && $tag eq "")
      {
	# $lastspaces already ok
	if(!($kinner=~m/^\s+$/s)) {
          $kinner=GitFriendly($kinner,$inner,substr($kfull,-1) eq "\n");
	}
      }
    }
    $buf.=$kfull.$kinner.$ctag;
  }
  return($buf);
}

sub RebuildXML {
  my($html,$newhtml)=@_;
  my($htmllc,$newhtmllc,$buf,$translatorset,$xml);
  my($i,$i2,$i3,$tagclosed,$tag,$ic2,$ic3,$inner,$ctag,$rb);
  my($metaname,$metacontent);
  my($tags,@tags,$endtags,$taglist,$taglistlc,$tagc,$it1,$it2,$it3,$tagval);
  my($podcast,$podcast_subtitle,@podcast);
  my($sidebar);


  Debug(2,"############ ORIG X[HTML] ################\n".$html."\n######################################\n\n");
  Debug(2,"############ NEW HTML ################\n".$newhtml,"\n######################################\n\n");
  Debug(2,"############ INPUT FIELDS ################\n");
  foreach $i (keys %in) {
    unless($i=~m/html/) {
      Debug(2,"$i = $in{$i}\n");
    }
  }
  Debug(2,"\n######################################\n\n");

  $html=~s/\r\n/\n/gs;
  $html=~s/\r/\n/gs;
  $newhtml=~s/\r\n/\n/gs;
  $newhtml=~s/\r/\n/gs;
  $newhtml=~s|<(br)>|<$1/>|gis; # Close BR tag: firefox remove it...
  $newhtml=~s|<(hr)>|<$1/>|gis; # Close HR tag: firefox remove it...
  $newhtml=~s|(<img.*?["'\s]+)>|$1/>|gis; # Fix unclosed "img" tag:
  $newhtml=~s|(\&nbsp;)+ +| |gis; # Remove &nbsp; (not accepted by xmllint)
  $newhtml=~s| +(\&nbsp;)+| |gis; # Remove &nbsp; (not accepted by xmllint)
  $newhtml=~s|(\&nbsp;)+| |gis; # Remove &nbsp; (not accepted by xmllint)

  # Fix absolute to relative links:
  $newhtml=~s|"$fsfebase|"|gis;
  $newhtml=~s|"$gitbase.*?/raw/branch/master|"|gis;

  $newhtml=CheckASplit($html,$newhtml);

  $htmllc=lc($html);

  # Extract tags (they are placed afterwards)
  ($tags)=($newhtml=~m|<ul class="tags">(.*?)</ul>|si);
  @tags=($tags=~m|(<li.*?><a.*?>.*?</a></li>)|gis);
  %newtags=();
  for($i=0;$i<=$#tags;$i++) {
    $_=$tags[$i];
    ($key)=m/key=["'](.*?)["']/si;
    $tags[$i]=~s|<li.*?><a.*?>||is;
    $tags[$i]=~s|</a></li>||is;
    $tags[$i]=~s|(<br/?>)+$||gis;
    $newtags{$key}=$tags[$i];
    Debug(1,"TAG $i = $tags[$i]\n");
  }
  %origtags=();
  foreach(split("\t",$in{origtags}))
  {
    ($tag,$value)=m/(\S+?)=(.*)/;
    $origtags{$tag}=$value;
  }

  # Extract podcast metadata
  ($podcast)=($newhtml=~m|<div id="podcast".*?>(.*?)</div>|si);
  ($podcast_subtitle)=($podcast=~m|<h5.*?><em>(.*?)</em></h5>|si);
  ($podcast)=($podcast=~m|<ul>(.*?)</ul>|si);
  @podcast=($podcast=~m|<li.*?<span.*?>(.*?)</span>|gis);

  # Extract sidebar
  ($sidebar)=($newhtml=~m|<aside id="sidebar".*?>(.*?)</aside>|si);

  # Remove header and footer from newhtml
  $newhtml=~s|<main><div id="content">\n?||gis;
  $newhtml=~s|<div id="article-metadata">.*?</div>\n?||gis;
  $newhtml=~s|<h2 id="webpreviewtags">.*||gis;


  #$newhtml=~s|<h2>&nbsp;</h2><footer id="tags"><h2>Tags</h2>\n?||gis;
  #$newhtml=~s|<aside id="sidebar"|</body>\n<sidebar|gis;
  #$newhtml=~s|</aside|</sidebar|gis;
  #$newhtml=~s|(</div>)?</main>\n?||gis;

  Debug(2,"############ NEW HTML [FIXED] ################\n".$newhtml,
	  "\n############ TAGS ####################\n".$tags,
	  "\n############ SIDEBAR #################\n".$sidebar,
	  "\n############ PODCAST #################\n".$podcast,
	  "\n######################################\n");

  $newhtmllc=lc($newhtml);

  $xml=0;
  $translatorset=0;
  $i=0;
  while(defined($i) && $i2>=0 && $i<length($htmllc)) {
    ($tag,$i2,$i3,$tagclosed)=GetNextTag(\$htmllc,$i);
    Debug(1,"TAG=$tag, $i, $i2, $i3, $tagclosed\n");
    if($i2<0) {
      $buf.=substr($html,$i);
      last;
    }
    $buf.=substr($html,$i,$i2-$i);
    $_=substr($html,$i2,$i3-$i2);
    $i=$i3;
    $inner="";
    $ctag="";
    $ic2=-1;
    $ic3="";
    if(!$tagclosed) {
      ($ic2,$ic3,$inner,$ctag)=GetCloseTagContent(\$htmllc,$tag,$i3,\$html);
    }

    if($tag eq "html") {
      if(m/newsdate/i && length($in{htmlnewsdate})>0) {
	s/newsdate=(["']).*?(["'])/newsdate=$1$in{htmlnewsdate}$2/si;
      }
    }
    elsif($tag eq "sidebar") {
      $buf.=$_;
      if(!$tagclosed) {
        $rb=RebuildBody($inner,$sidebar);
        $buf.=$rb.$ctag;
        $i=$ic3;
      }
      $_="";
    }
    elsif($xml && $tag=~m/^(description|text|body)$/i) {
      $buf.=$_;
      $newhtml=~s|^.*?<nobody>||is;
      $newhtml=~s|</nobody>.*||is;
      if($tag ne "body") {
        # Remove first and last paragraph marks
        $newhtml=~s|^<p.*?>||is;
        $newhtml=~s|</p>$||is;
      }
      $rb=RebuildBody($inner,$newhtml);
      $buf.=$rb.$ctag;
      $i=$ic3;
      $_="";
    }
    elsif($tag eq "body") {
      $buf.=$_;
      $rb=RebuildBody($inner,$newhtml);
      Debug(3,"++NEWHTML=$newhtml\n",
              "++RB=$rb\n",
              "++ENDTAG=$ctag\n",
              "++END\n");
      $buf.=$rb.$ctag;
      $i=$ic3;
      $_="";
    }
    elsif($tag=~m|/\S+set$| || $tag eq "/feed" || $tag eq "/data") {
      # Do nothing, just to avoid the next to be true (should not be useful since all handling
      # was done inside opening tag)
    }
    elsif($tag=~m|\S+set$| || $tag eq "feed" || $tag eq "data") {
      # pure XML file
      $xml=1;
      if($tag eq "data")
      {
	$buf.=$_;
	$buf.=RebuildBody($inner,$newhtml).$ctag;
	$i=$ic3;
	$_="";
      }
    }
    # Handle tags outside body/data/text
    elsif($tag eq "meta") {
      ($metaname)=m/name="(.*?)"/si;
      if(defined($metacontent=$in{"meta_$metaname"})) {
        s/content="(.*?)"/content="$metacontent"/si;
      }
    }
    elsif($tag eq "translator" && $tagclosed==0) {
      $i=$ic3;
      if($in{translator} eq "") {
        $_="";
      }
      else {
        $translatorset=1;
        $_.=$in{translator}.$ctag;
      }
    }
    elsif($tag eq "version") {
      $_.=$in{version}.$ctag;
      $i=$ic3;
    }
    elsif($tag eq "description") {
      $_.=$in{meta_description}.$ctag;
      $i=$ic3;
    }
    elsif($tag eq "title") {
      $_.=$in{title}.$ctag;
      $i=$ic3;
    }
    elsif($tag eq "/html") {
      if(length($in{translator})>0) {
        if(!$translatorset) {
	  $_="<translator>$in{translator}</translator>\n".$_;
	}
      }
    }
    elsif($tag eq "tags")
    {
      $tags=$_;
      $endtags=$ctag;
      $taglist=$inner;
      $taglistlc=lc($inner);
      $i=$ic3;
      $it1=0;
      $it2=0;
      $tagc=0;
      %tags=(); # To update tag translation memory
      while($it2>=0 && defined($it1)) {
	($tag,$it2,$it3,$tagclosed)=GetNextTag(\$taglistlc,$it1);
	if($it2<0) {
	  last;
	}
        $tags.=substr($taglist,$it1,$it2-$it1);
	$_=substr($taglist,$it2,$it3-$it2);
	if($tagclosed) {
	  $it1=$it3;
	}
	else {
	  ($ic2,$ic3,$inner,$ctag)=GetCloseTagContent(\$taglistlc,$tag,$it3,\$taglist);
	  if($ic2>=0) {
	    if($tag eq "tag") {
	      ($key)=m/key=["'](.*?)["']/si;
	      if(length($key)>0 && defined($newtags{$key})) {
	        $_.=$newtags{$key};
	      }
	      else {
	        $_.=$tags[$tagc]; # Shold never happens...
	      }
	      $_.=$ctag;
	      if(length($newtags{$key})>0 && $origtags{$key} ne $newtags{$key}) {
	        $tags{$key}=$newtags{$key};
	      }
	    }
	    else {
	      $_=substr($taglist,$it2,$ic3-$it2);
	    }
	  }
	  else {
	    $ic3=$it3;
	    $_="";
	  }
	  $it1=$ic3;
	}
	if($tag eq "tag") {
          $tagc++;
        }
	$tags.=$_;
      }
      $tags.=substr($taglist,$it1);
      $_=$tags.$endtags;
    }
    elsif($tag eq "podcast")
    {
      $tags=$_;
      $endtags=$ctag;
      $taglist=$inner;
      $taglistlc=lc($inner);
      $i=$ic3;
      $it1=0;
      $it2=0;
      $tagc=0;
      while($it2>=0 && defined($it1)) {
	($tag,$it2,$it3,$tagclosed)=GetNextTag(\$taglistlc,$it1);
	if($it2<0) {
	  last;
	}
        $tags.=substr($taglist,$it1,$it2-$it1);
	if($tagclosed || ($tag=~m/chapters/)) {
	  $_=substr($taglist,$it2,$it3-$it2);
	  $it1=$it3;
	}
	else {
	  ($ic2,$ic3)=GetCloseTagContent(\$taglistlc,$tag,$it3);
	  $_=substr($taglist,$it2,$ic3-$it2);
	  $it1=$ic3;
	}
	if($tag eq "subtitle")
	{
	  s|>(.*?)<|>$podcast_subtitle<|is;
	}
	elsif($tag eq "chapter")
	{
	  s|>(.*?)<|>$podcast[$tagc]<|si;
	  $tagc++;
	}
	$tags.="$_";
      }
      $tags.=substr($taglist,$it1);
      $_=$tags.$endtags;
    }
    elsif($tag eq "image")
    {
      if(length($in{imageurl})>0) {
        s/(url=\s*")(.*?)(")/$1$in{imageurl}$3/si;
      }
      ($curralt)=m/(alt=\s*".*?")/si;
      if(length($curralt)==0) {
        if(length($in{imagealt})>0) {
           s/(\surl=\s*")/ alt="$in{imagealt}"$1/si;
        }
      }
      else {
        s/(alt=\s*")(.*?)(")/$1$in{imagealt}$3/si;
      }
    }
    Debug(1,$_,"\n");
    $buf.=$_;
  }
  return($buf);
}

sub IncL {
  my($buf,$j,$w,$i,$tag,$sp,$addsp)=@_;
  my($add);
  if(ref($sp) ne "ARRAY")
  {
    $sp=[];
  }
  if($$j<$i) {
    if(length($addsp)>0)
    {
      $lastword=$w->[$$j];
      if(!($lastword=~m/^[\.,;:\?!]$/)) {
	if(length($lastsp)==0 && length($add)==0) { $$buf.=$addsp; }
	$$buf.=$lastsp;
        $add=$lastsp;
      }
      $lastsp="";
    }
    $$buf.="<$tag>";
    while($$j<$i) {
      $lastword=$w->[$$j];
      if(length($addsp)>0) {
	if(!($lastword=~m/^[\.,;:\?!]$/)) {
	  if(length($lastsp)==0 && length($add)==0) { $$buf.=$addsp; }
	  $$buf.=$lastsp;
	}
      }
      if($tag ne "del" || substr($lastword,0,1) ne "<") {
        $$buf.=$lastword;
      }
      if(length($addsp)>0) {
        $lastsp=$sp->[$$j];
      }
      $$j++;
    }
    #if(length($add)>0) {
    #  $$buf=substr($$buf,0,0-length($add));
    #}
    $$buf.="</$tag>";
  }
}

sub WordComparison {
  my ($w1,$w2)=@_;
  my (@w1,@w2);
  my ($s1,$s2,$len,$max,$buf,$i,$j,$k);
  #print STDERR "W1=$w1 W2=$w2\n";
  @w1=split("",$w1);
  @w2=split("",$w2);
  if($#w1<$#w2) { $len=$#w1+1; $max=$#w2+1; }
  else { $len=$#w2+1; $max=$#w1+1; }
  if($len<=1) { return(undef); }
  ( $s1, $s2 ) = LCSidx( \@w1, \@w2);
  if(($#{$s1}+1)<=($len/2)) { return(undef); }
  if($len<=($max/2) && ($#{$s1}+1)<$len) { return(undef); }
  $i=$j=$k=0;
  push(@{$s1},$#w1+1);
  push(@{$s2},$#w2+1);
  while($i<=$#{$s1}) {
    while($i<=$#{$s1} && $s1->[$i]==$j && $s2->[$i]==$k) {
      $buf.="$w1[$s1->[$i]]";
      $i++;
      $j++;
      $k++;
    }
    if($j==0) { IncL(\$buf,\$j,\@w1,$s1->[$i],"del"); }
    IncL(\$buf,\$k,\@w2,$s2->[$i],"ins");
    IncL(\$buf,\$j,\@w1,$s1->[$i],"del");
  }
  # Limit to only 1 delete:
  @w1=($buf=~m/<del>/g);
  if($#w1>=1) { return(undef); }

  # If 1 delete is present, allow only 1 insert:
  @w2=($buf=~m/<ins>/g);
  if($#w1==0 && $#w2>=1) { return(undef); }

  return($buf);
}

sub SplitSpacePoint {
  my $s=shift;
  my ($i,$k,$b1,$b2,$b3,$p1,$p2,$p3,@w,@sp,$lastspace);
  $_=$s;
  $lastspace=0;
  $i=0;
  while(length($_)>0) {
    #$p1=$p2=$p3=$b1=$b2=$b3="";
    #($p1,$p2,$p3)=m/(.*?)([\.,;:\?"'«»!])(.*)/s;
    ($p1,$p2,$p3)=m/(.*?)(['\.,;:\?!\<\>])(.*)/s;
    ($b1,$b2,$b3)=m/(.*?)(\s+)(.*)/s;
    #print "\$_=$_\n";
    #print "P=$p1, 2=$p2, 3=$p3\n";
    #print "B=$b1, 2=$b2, 3=$b3\n";
    if((length($p1.$p2)>0 && length($p1)<length($b1))||(length($b2)==0)) {
      $b1=$p1;
      $b2=$p2;
      $b3=$p3;
    }
    if(length($b1.$b2)==0) { $b1=$_; }
    if(substr($b1,0,1) eq "<") {
      $b1="";
      $b2=substr($_,0,1);
      $b3=substr($_,1);
    }
    if($b2=~m/\s/s) { $sp[$i]=$b2; $b2=""; }
    else { $sp[$i]=""; }
    if(length($b1)>0) {
      $w[$i]=$b1;
      #if(length($b2)==0) { $sp[$i]=" "; }
      $i++;
    }
    if(length($b2)>0) {
      if($b2 eq "<") {
        $k=index($_,">");
	if($k>0) {
	  $b2=substr($_,length($b1),$k-length($b1)+1);
          $w[$i]=$b2;
	  $_=substr($_,$k+1);
          ($b2,$b3)=m/^(\s+)(.*)/s;
	  $sp[$i]=$b2;
	  if(length($b3)==0) { $b3=$_; }
	  $b2="";
          $i++;
	}
      }
      if(length($b2)>0) {
	$w[$i]=$b2;
	$sp[$i]="";
	#if(length($b1)==0 && $lastspace) {
	#  $sp[$i]="b";
	#}
	if($b3=~m/^\s/s) {
	  $sp[$i].=" ";
	}
	$lastspace=0;
      }
      $i++;
    }
    else {
      $lastspace=1;
    }
    $_=$b3;
  }
  return((\@w,\@sp));
}

sub SplitSectionWords
{
  my $s=shift;
  my ($i);
  for($i=0;$i<=$#{$s};$i++) {
      ($w,$sp)=SplitSpacePoint($s->[$i]{buf});
      @{$s->[$i]{w}}=@{$w};
      @{$s->[$i]{sp}}=@{$sp};
  }
}

sub OrderCont {
  my($tagn,$idxn,$idxo)=@_;
  my(@os,$i,$k);
  $i=0;
  $k=0;
  while($i<=$#$idxn) {
    while($k<$idxn->[$i]) {
      $k++;
      push(@os,-1);
    }
    if($k<=$tagn) {
      push(@os,$idxo->[$i]);
    }
    $i++;
    $k++;
  }
  return(@os);
}

sub Compare
{
  my($w1,$w2,$empty)=@_;
  my($i1,$i2,$max,$res,$a1,$a2);
  if(!defined($empty)) { $empty=1; }
  $i1=$#{$w1};
  $i2=$#{$w2};
  if($i1<0 && $i1==$i2) {
    # Empty section, keep it
    return($empty);
  }
  if($i2>$i1) { $max=$i2; }
  else { $max=$i1; }
  ( $a1, $a2 ) = LCSidx( $w1, $w2);
  if($i1==$i2 && $#{$a1}==$i1) {
    # 100% equal, keep it
    return(1);
  }
  $max++;
  return(($#{$a1}+1)/$max);
}

sub LoopCompare
{
  my($secuno,$secdue,$i,$k)=@_;
  my($j,$k2,$p);
  if(!defined($k)) { $k=-1; }
  for($k2=0;$k2<=$#{$secdue};$k2++) {
    if($k2!=$k && $secdue->[$k2]{p}<1) {
      $p=Compare($secuno->[$i]{w},$secdue->[$k2]{w},-1);
      if($p>$secuno->[$i]{p} && $p>$secdue->[$k2]{p}) {
	#Debug(4,"$i/$k2] $p > ($secuno->[$i]{p} | $secdue->[$k2]{p}) $j [".substr($secuno->[$j]{buf},0,20)." | ".substr($secdue->[$k2]{buf},0,20)."] \n");
        $j=$secdue->[$k2]{link};
	$secuno->[$i]{p}=$p;
	$secuno->[$i]{link}=$k2;
	if($p==1) { last; }
      }
    }
  }
  if(length($k2=$secuno->[$i]{link})) {
    if(length($j=$secdue->[$k2]{link}))
    {
      if($i!=$j) {
	# Unlink the old one
	$secuno->[$j]{link}=undef;
	$secuno->[$j]{p}=undef;
      }
    }
    $secdue->[$k2]{p}=$secuno->[$i]{p};
    $secdue->[$k2]{link}=$i;
    if($k!=$k2) {
      # Unlink the old one
      $secdue->[$k]{link}=undef;
      $secdue->[$k]{p}=undef;
    }
  }
}

sub DiffHTML
{
  my ($uno,$due)=@_;
  my ($w,$sp,@uno,@due,@sp1,@sp2,$s1,$s2,$i,$j,$k,$buf,$j2,$k2,$lastsim,$p);
  my ($lasti, $lastj, $lastk);
  my (@taguno,@tagdue,@orduno,@orddue);
  my (@secnew,$s,$tagfull,$buffull,$buflocal);
  $uno=decode('utf8',$uno);
  $due=decode('utf8',$due);
  my @secuno=SplitSections($uno);
  my @secdue=SplitSections($due);
  @taguno=GetSecTags(@secuno);
  @tagdue=GetSecTags(@secdue);

  # Check if some html tag[s] are missing/added
  ( $s1, $s2 ) = LCSidx( \@taguno, \@tagdue );
  push(@{$s1},$#taguno+1);
  push(@{$s2},$#tagdue+1);

  SplitSectionWords(\@secuno);
  SplitSectionWords(\@secdue);

  @orduno=OrderCont($#taguno,$s1,$s2);
  @orddue=OrderCont($#tagdue,$s2,$s1);

  if(($#taguno+1)!=$#{$s1} || $#taguno!=$#tagdue)
  {
    # Some section were added and/or removed. Try to guess which.
    # First of all, check the associated ones:
    for($i=0;$i<=$#secdue;$i++) {
      if(($k=$orddue[$i])>=0) {
	$p=Compare($secdue[$i]{w},$secuno[$k]{w});
	$secdue[$i]{p}=$secuno[$k]{p}=$p;
	$secdue[$i]{link}=$k;
        $secuno[$k]{link}=$i;
      }
    }
    for($i=0;$i<=$#secdue;$i++) {
      $k=$secdue[$i]{link};
      if(length($k)>0 && $k>=0) {
	if($secdue[$i]{p}==1) {
	  # All already ok
	  next;
	}
	LoopCompare(\@secdue,\@secuno,$i,$k);
	$k=$secdue[$i]{link};
      }
    }
    # Then, try the ones not linked, in both arrays
    for($i=0;$i<=$#secdue;$i++) {
      $k=$secdue[$i]{link};
      if(length($k)==0) {
	LoopCompare(\@secdue,\@secuno,$i);
      }
    }
    for($i=0;$i<=$#secuno;$i++) {
      if(length($secuno[$i]{link})==0) {
    	LoopCompare(\@secuno,\@secdue,$i);
      }
    }

    # To end the process, compose the list.
    #
    # Add the missing section to the new(due) array
    $k=-1;
    @orduno=();
    for($i=0;$i<=$#secuno;$i++) {
      if(length($secuno[$i]{link})==0) {
        if($k<0) { push(@orduno,$i); }
	else { $secdue[$k]{miss}.="$i "; }
      }
      else {
        $k=$secuno[$i]{link};
      }
    }
    # Generate the order array
    @orddue=();
    $s=0;
    for($j=0;$j<=$#orduno;$j++) {
      $orddue[$s++]="$orduno[$j] -1";
    }
    for($i=0;$i<=$#secdue;$i++) {
      $k=$secdue[$i]{link};
      if(length($k)==0) { $k=-1; }
      $orddue[$s++]="$k $i";
      if(length($k=$secdue[$i]{miss})) {
        @orduno=split(" ",$k);
	for($j=0;$j<=$#orduno;$j++) {
	  $orddue[$s++]="$orduno[$j] -1";
	}
      }
    }
  }
  else {
    # Exactly the same sections, compose the array
    @orddue=();
    for($i=0;$i<=$#secdue;$i++) {
      $orddue[$i]="$i $i";
    }
  }

  # Generate a single array with both versions combined
  for($s=0;$s<=$#orddue;$s++) {
    ($j,$k)=split(' ',$orddue[$s]);
    if($j<0) {
      $secnew[$s]{tag}=$secdue[$k]{tag};
      $secnew[$s]{pre}=$secdue[$k]{pre};
      $secnew[$s]{ctag}=$secdue[$k]{ctag};
      $secnew[$s]{nodiff}=$secdue[$k]{nodiff};
      $secnew[$s]{tagfull1}=undef;
      $secnew[$s]{tagfull2}=$secdue[$k]{tagfull};
      $secnew[$s]{buf1}=undef;
      $secnew[$s]{buf2}=$secdue[$k]{buf};
    }
    elsif($k<0) {
      $secnew[$s]{tag}=$secuno[$j]{tag};
      $secnew[$s]{pre}=$secuno[$j]{pre};
      $secnew[$s]{ctag}=$secuno[$j]{ctag};
      $secnew[$s]{nodiff}=$secuno[$j]{nodiff};
      $secnew[$s]{tagfull2}=undef;
      $secnew[$s]{tagfull1}=$secuno[$j]{tagfull};
      $secnew[$s]{buf2}=undef;
      $secnew[$s]{buf1}=$secuno[$j]{buf};
    }
    else {
      $secnew[$s]{tag}=$secdue[$k]{tag};
      $secnew[$s]{pre}=$secdue[$k]{pre};
      $secnew[$s]{ctag}=$secdue[$k]{ctag};
      $secnew[$s]{nodiff}=$secdue[$k]{nodiff};
      $secnew[$s]{tagfull1}=$secuno[$j]{tagfull};
      $secnew[$s]{tagfull2}=$secdue[$k]{tagfull};
      $secnew[$s]{tagfull}=$secdue[$k]{tagfull};
      $secnew[$s]{buf1}=$secuno[$j]{buf};
      $secnew[$s]{buf2}=$secdue[$k]{buf};
    }
  }

  $buffull="";
  for($s=0;$s<=$#secnew;$s++)
  {
    $tagfull=$secnew[$s]{tagfull2};if(length($tagfull)==0) { $tagfull=$secnew[$s]{tagfull1}; }
    $buflocal=$secnew[$s]{buf2};if(length($buflocal)==0) { $buflocal=$secnew[$s]{buf1}; }
    $buffull.=$secnew[$s]{pre};
    if($secnew[$s]{tag} eq "img")
    {
      $secnew[$s]{nodiff}=1;
      #if(length($secnew[$s]{buf1})>0) { $buflocal=$secnew[$s]{buf1}; }
      #$_=$secnew[$s]{tagfull1};
      #($secnew[$s]{buf1})=m/alt=\s*"(.*?)"/;
      #$_=$secnew[$s]{tagfull2};
      #($secnew[$s]{buf2})=m/alt=\s*"(.*?)"/;
    }

    if($secnew[$s]{nodiff})
    {
      $buf=$buflocal;
    }
    else
    {
      ($w,$sp)=SplitSpacePoint($secnew[$s]{buf1});
      @uno=@{$w};
      @sp1=@{$sp};
      ($w,$sp)=SplitSpacePoint($secnew[$s]{buf2});
      @due=@{$w};
      @sp2=@{$sp};

      ( $s1, $s2 ) = LCSidx( \@uno, \@due );

      $i=0;
      $j=0;
      $k=0;
      $buf="";
      push(@{$s1},$#uno+1);
      push(@{$s2},$#due+1);
      $lastsp="";
      $lasti=$lastj=$lastk=-1;
      while($i<=$#{$s1} && ($lasti!=$i || $lastj!=$j || $lastk!=$k)) {
	# Just to avoid infinite-loop bugs
        $lasti=$i;
        $lastj=$j;
        $lastk=$k;

	while($i<=$#{$s1} && $s1->[$i]==$j && $s2->[$i]==$k) {
	  $buf.=$lastsp.$uno[$j];
	  $lastsp=$sp1[$j];
	  $i++;
	  $j++;
	  $k++;
	}
	$k2=$k;
	$lastsim=-1;
	while($k2<$s2->[$i])
	{
	  if(substr($due[$k2],0,1) eq "<") {
	    $k2++;
	    next;
	  }
	  $j2=$j;
	  while($j2<$s1->[$i] && $k2<$s2->[$i])
	  {
	    #print STDERR "$j2 $s1->[$i] - $k2 $s2->[$i]\n";
	    # Word comparison: check any similar word
	    $word=WordComparison($uno[$j2],$due[$k2]);
	    if(defined($word)) {
	      if(!$lastsim) {
		IncL(\$buf,\$j,\@uno,$j2,"del",\@sp1," ");
		IncL(\$buf,\$k,\@due,$k2,"ins",\@sp2," ");
	      }
	      $buf.=$lastsp.$word;
	      $lastsp=$sp1[$j2];
	      $lastsim=1;
	      $j++;
	      $k++;
	      last;
	    }
	    else {
	      $lastsim=0;
	    }
	    $j2++;
	  }
	  $k2++;
	}
	if($uno[$j]=~m/^</ && substr($uno[$j],0,3) eq substr($due[$k],0,3) && $j<$s1->[$i] && $k<$s2->[$i])
	{
	  # Skip tags such as <img or <a with different internal fields
	  $buf.=$due[$k];
	  $j++;
	  $k++;
	}
	else {
	  IncL(\$buf,\$j,\@uno,$s1->[$i],"del",\@sp1," ");
	  IncL(\$buf,\$k,\@due,$s2->[$i],"ins",\@sp2," ");
        }
      }
      if($secnew[$s]{tag} eq "img")
      {
	$buf=~s/</\&lt;/g;
	$buf=~s/>/\&gt;/g;
	$buf=~s/"/\&quot;/g;
	$tagfull=~s/(alt=\s*")(.*?)(")/$1$buf$2/i;
      }
    }
    $buffull.=$tagfull.$buf.$secnew[$s]{ctag};
  }

  return(encode('utf8',$buffull));
}

sub AddWebpreview
{
  my $s="wp";
  if($padid ne "" && $padid ne "--") {
    $s.=" $padid/".int($review);
  }
  $s.=" - ";
  return($s);
}

sub AddImgAltHandling {
  my($s)=@_;
  if($s=~m|alt=\s*"|si) {
    # Add title to check translation of the ALT
    ($alt)=($s=~m/alt=\s*"(.*?)"/si);
    #if(length($alt)>0) {
      #$s=~s/alt="/title="$alt" alt="/;
      #$alt=~s/\&lt;/</gi;
      #$alt=~s/\&gt;/>/gi;
      #$s="<span class=\"imgalt\"$editable>$alt<br></span>\n".$s;
      $s=~s/(alt=\s*".*?")/$1 title="$alt" onClick="ChangeAlt(this)"/si;
    #}
  }
  else {
    $s=~s/(<img)/$1 onClick="ChangeAlt(this)"/;
  }
  #$s=~s/(src=\s*")/onContextMenu="return ChangeSrc(this)" $1/si;
  return($s);
}

sub FixBase {
  my ($s)=@_;
  my($i,$i2,$i3,$tag,$tagclosed,$sl,$s2,$img);
  if(length($imagebase)>0) {
    $s=~s|(src=\s*)"/|$1"$imagebase/|gsi;
  }
  if(length($fsfehost)>0) {
    $s=~s|(href=\s*)"/|$1"$fsfehost/|gsi;
  }
  # Check if there is some images
  $i=0;
  $sl=lc($s);
  $s2=$s;
  $s="";
  while($i>=0) {
    if(($i=index($sl,"<img",$i))>=0) {
      ($tag,$i2,$i3,$tagclosed)=GetNextTag(\$sl,$i);
      if($i2>=0 && $i3>=$i2) {
        $s.=substr($s2,0,$i2);
	$img=substr($s2,$i2,$i3-$i2);
	$s2=substr($s2,$i3);
	$sl=substr($sl,$i3);
	$s.=AddImgAltHandling($img);
	$i3=0;
      }
      else { $i3=-1; }
      $i=$i3;
    }
  }
  $s.=$s2;
  if($contexthref)
  {
    # Check if there is some anchor
    $i=0;
    $sl=lc($s);
    $s2=$s;
    $s="";
    while($i>=0) {
      $i=index($sl,"<a ",$i);
      if($i<0) { $i=index($sl,"<a\n",$i); }
      if($i>=0) {
	($tag,$i2,$i3,$tagclosed)=GetNextTag(\$sl,$i);
	if($i2>=0) {
	  $s.=substr($s2,0,$i2);
	  $img=substr($s2,$i2,$i3-$i2);
	  $s2=substr($s2,$i3);
	  $sl=substr($sl,$i3);
	  $img=~s/(href=\s*")/onContextMenu="return ChangeHref(this)" $1/si;
	  $s.=$img;
	  $i3=0;
	}
	else { $i3=-1; }
	$i=$i3;
      }
    }
    $s.=$s2;
  }
  return($s);
}


sub NewID
{
  $webid++;
  $newid=sprintf(" id=\"$idprefix".sprintf("%03d",$webid)."$idsuffix\"");
}

sub AddID {
  my($s)=@_;
  {
    my($i);
    NewID();
    if($s=~m/\s+id=/) {
      if(length($idsuffix)>0) {
        $s=~s/( id=['"].*?)(['"])/$1$idsuffix$2/si;
      }
    }
    else {
      if($s=~m/ contenteditable=/si) {
        $s=~s/( contenteditable=)/$newid$1/si;
      }
      else {
        $s=~s/ /$newid /si;
      }
    }
    #$i=rindex($s,">");
    #if($i>0) {
    #  $s=substr($s,0,$i)." id=\"".sprintf("%03d",$i)."\"".substr($s,$i);
    #}
  }
  return($s);
}

sub FixTitle {
  my $title=shift;
  $title=~s/\n/ /gs;
  $title=~s/\t+/ /gs;
  $title=~s/ +/ /gs;
  $title=~s/^ | $//gs;
  return($title);
}

sub GenerateHTML
{
  my($html,$idp,$ids)=@_;
  my($buf,$i,$i2,$i3,$ic2,$ic3);
  my($inner,$ctag,$taglist,$taglistlc,$it1,$it2,$it3);

  $webid=0;
  $idprefix=$idp;
  $idsuffix=$ids;

  $newsdate="";
  $htmlnewsdate="";
  $author="";
  $translator="";
  $version="";
  $image="";
  %meta=();


  $tags="";
  $podcast="";
  $buf="";
  $body=0;
  $sidebar="";
  $isside=0;
  $xml=0;
  $pcount=0;
  $issidebar=0;
  $title_ok=0;
  $titlexml="";
  $titlehtml="";

  $htmllc=lc($html);

  Debug(15,"HTML TO SCAN= $html\n");
  # Scan the whole html
  $i=0;
  $i2=0;
  while(defined($i) && $i2>=0) {
    ($tag,$i2,$i3,$tagclosed)=GetNextTag(\$htmllc,$i);
    if($i2<0) {
      $buf.=substr($html,$i);
      last;
    }
    Debug(4,"GN = $tag - $i - $i2 - $i3 - $tagclosed\n");
    if($issidebar) {
      $sidebar.=FixBase(substr($html,$i,$i2-$i));
    } else {
      $buf.=FixBase(substr($html,$i,$i2-$i));
    }
    if($tag eq "?xml") {
      # Good xml!!! But do not add it to the buffer
      if(substr($html,$i3,1) eq "\n") { $i3++; }
      $i=$i3;
      next;
    }
    $_=substr($html,$i2,$i3-$i2);
    $i=$i3;

    # Fix for testimonial handling
    if($tag eq "html") {
      if(m/newsdate/i) {
	($newsdate)=m/newsdate=["'](.*?)["']/si;
	$htmlnewsdate=$newsdate;
      }
    }
    elsif($tag eq "original") {
      ($newsdate)=m/content=["'](.*?)["']/si;
    }
    elsif($tag eq "author") {
      ($author)=m/id=["'](.*)["']/si;
    }
    elsif($tag eq "meta") {
      ($metaname)=m/name="(.*?)"/si;
      ($metacontent)=m/content="(.*?)"/si;
      $metacontent=~s/\s\s+/ /g;
      $meta{$metaname}=$metacontent;
    }
    elsif($tag eq "translator" && $tagclosed==0) {
      ($ic2,$ic3,$translator)=GetCloseTagContent(\$htmllc,$tag,$i3,\$html);
      $i=$ic3;
      $_="";
    }
    elsif($tag=~m|/\S+set$| || $tag eq "/feed" || $tag eq "/data") {
      $_="<h2 id=\"webpreviewtags\">\&nbsp;</h2><replacetags/>\n</div>\n<replaceside/>\n</main><replacepod/></body>\n</html>";
    }
    elsif($tag=~m|\S+set$| || $tag eq "feed" || $tag eq "data") {
      # pure XML file
      $_="<html>\n<head>\n".AddCss()."</head>";
      $_.="<body onLoad=\"LoadBody()\" class=\"news press release\"><main><div id='content$idsuffix'>";
      $body+=2;
      $xml=1;
    }
    elsif($xml && ($tag eq "title" || $tag eq "name")) {
      ($ic2,$ic3,$titlexml)=GetCloseTagContent(\$htmllc,$tag,$i3,\$html);
      $i=$ic3;
      $_=AddID("<h1 id=\"ttttt\"$editable>")."$titlexml</h1>\n";
      $title_ok=1;
      $titlexml="<title>".AddWebpreview()."$titlexml</title>";
    }
    elsif($xml && ($tag=~m/body$/)) {
      # Replace body tag
      s/body>/nobody>/gi;
    }
    elsif($xml && ($tag eq "description" || $tag eq "text")) {
      ($ic2,$ic3,$inner)=GetCloseTagContent(\$htmllc,$tag,$i3,\$html);
      $_=AddID("<nobody><p$editable id=\"$tag\">").$inner."</p></nobody>\n";
      $i=$ic3;
    }
    elsif($tag eq "description") {
      ($ic2,$ic3,$meta{description})=GetCloseTagContent(\$htmllc,$tag,$i3,\$html);
      $_="";
      $i=$ic3;
    }
    elsif($tag eq "title") {
      ($ic2,$ic3,$titlehtml,$ctag)=GetCloseTagContent(\$htmllc,$tag,$i3,\$html);
      $_.=AddWebpreview().$titlehtml.$ctag;
      $i=$ic3;
    }
    elsif($tag eq "body") {
      #s|<body>|<body class=" press release">|;
      if(!m|class=|si) {
	s|<body|<body class="news press release"|si;
      }
      elsif(!m/class=".*?(news|frontpage|subsite|toplevel|overview).*?"/) {
	s|class="|class="news |si;
      }
      s|<body|<body onLoad="LoadBody()"|si;
      #$_.="<section id='main' role='main'><article id='content$idsuffix'>";
      $_.="<main><div id='content$idsuffix'>";

      $body++;
    }
    elsif($tag eq "/body") {
      #s|</body>|<h2>\&nbsp;</h2></article>|;
      #s|</body>|<h2>\&nbsp;</h2></div>\n|;
      s|</body>|<h2 id="webpreviewtags">\&nbsp;</h2><replacetags/>\n</div>\n<replaceside/>\n</main><replacepod/>\n</body>\n|si;
    }
    elsif($tag=~m/^($tags_main)$/i) {
      ($ic2,$ic3)=GetCloseTagContent(\$htmllc,$tag,$i3);
      $_=substr($html,$i2,$i3-$i2);
      $i=$ic3;
      if($tag eq "p") {
	if(m|<p\s+id=.category|) {
	  if($pcount==0) { $pcount--; }
	}
      }
      s/<($tags_main)/<$1$editable/is;
      if(m/<h1/) {
	if(!$title_ok) {
          $titleh1=substr($html,$i3,$ic2-$i3);
	  if(FixTitle($titleh1) eq FixTitle($titlehtml))
	  {
            $title_ok=1;
            s/<h1/<h1 id="ttttt"/gis;
	  }
	  else
	  {
	    $title_ok=-1;
	  }
	}
      }
      if($tag eq "p") {
	  if($pcount==0) {
	    s/<p/<p id="teaser"/gis;
	    $pcount++;
	  }
	  $pcount++;
      }
      $_=FixBase(AddID($_).substr($html,$i3,$ic3-$i3));
      if($tag eq "h1") {
	$_.="\n<div id='article-metadata'>\n";
	$_.="<replaceauthor/>";
	$_.="</div>";
      }
    }
    elsif($tag eq "head") {
      $body++;
      $_.=AddCss();
    }
    elsif($tag eq "sidebar")
    {
      #if(!$tagclosed) {
      #  ($ic2,$ic3)=GetCloseTagContent(\$htmllc,$tag,$i3);
      #  $_=substr($html,$i2,$ic3-$i2);
      #  $i=$ic3;
      #}
      s|<sidebar|<aside id="sidebar"|si;
      $sidebar=FixBase($_);
      $_="";
      if(!$tagclosed) { $issidebar=1; }
    }
    elsif($tag eq "/html")
    {
      $issidebar=0;
    }
    elsif($tag eq "/sidebar")
    {
      s|</sidebar>|</aside>|si;
      $sidebar.=FixBase($_);
      $_="";
      $issidebar=0;
    }
    elsif($tag eq "version") {
      ($ic2,$ic3,$version,$ctag)=GetCloseTagContent(\$htmllc,$tag,$i3,\$html);
      $i=$ic3;
      if($xml) {
	$_.=$ctag; # Leave empty version tag for rebuild
      }
      else {
	$_="";
      }
    }
    elsif($tag eq "img")
    {
      Debug(14,"IMG $_\n");
      if(!$tagclosed) {
	($ic2,$ic3)=GetCloseTagContent(\$htmllc,$tag,$i3);
	$_=AddID(substr($html,$i2,$i3-$i2)).substr($html,$i3,$ic3-$i3);
	$i=$ic3;
      }
      else {
        $_=AddID($_);
      }
      Debug(14,"FIX $_\n");
      $_=FixBase($_);
    }
    elsif($tag eq "peertube")
    {
      if(!$tagclosed) {
	($ic2,$ic3)=GetCloseTagContent(\$htmllc,$tag,$i3);
	$_=substr($html,$i2,$ic3-$i2);
	$i=$ic3;
      }
      # <peertube url="https://media.fsfe.org/w/nCFNDeRxh3QpQoEMZd3w5W" />
      $fulltag=HTMLQuote($_);
      ($url)=m/ url=["'](\S+)["']/;
      $url=substr($url,rindex($url,"/")+1);
      # Add video tag
      $_="<video peertube=\"$fulltag\" crossorigin=\"crossorigin\" poster=\"https://download.fsfe.org/videos/peertube/$url.jpg\" controls=\"controls\">\n"
         ."  <source type='video/mp4; codecs=\"avc1.42E01E, mp4a.40.2\"' media=\"screen and (min-width:1200px)\" src=\"https://download.fsfe.org/videos/peertube/${url}_1080p.mp4\"></source>\n"
	 ."  <source type='video/mp4; codecs=\"avc1.42E01E, mp4a.40.2\"' media=\"screen and (max-width:1199px)\" src=\"https://download.fsfe.org/videos/peertube/${url}_720p.mp4\"></source>\n"
	 ."  <source type='video/mp4; codecs=\"avc1.42E01E, mp4a.40.2\"' media=\"screen and (max-width:420px)\" src=\"https://download.fsfe.org/videos/peertube/${url}_360p.mp4\"></source>\n"
	 ."  <source type='video/webm; codecs=\"vp9, opus\"' media=\"screen and (min-width:1200px)\" src=\"https://download.fsfe.org/videos/peertube/${url}_1080p.webm\"></source>\n"
	 ."  <source type='video/webm; codecs=\"vp9, opus\"' media=\"screen and (max-width:1199px)\" src=\"https://download.fsfe.org/videos/peertube/${url}_720p.webm\"></source>\n"
	 ."  <source type='video/webm; codecs=\"vp9, opus\"' media=\"screen and (max-width:420px)\" src=\"https://download.fsfe.org/videos/peertube/${url}_360p.webm\"></source>\n"
	 ."</video>\n";
  
    }
    elsif($tag eq "tags")
    {
      ($ic2,$ic3,$taglist)=GetCloseTagContent(\$htmllc,$tag,$i3,\$html);
      $taglistlc=lc($taglist);
      $i=$ic3;
      #$_='<aside id="tags">Tags:'."\n".'<ul class="taglist">'."\n";
      $tags="<footer id=\"tags\"><h2>Tags</h2>\n<ul class=\"tags\">\n";
      $it1=0;
      $it2=0;
      %tags=();
      %tagsid=();
      while($it2>=0) {
	($tag,$it2,$it3,$tagclosed)=GetNextTag(\$taglistlc,$it1);
	if($it2<0) {
	  last;
	}
	if($tagclosed) {
	  $_=substr($taglist,$it2,$it3-$it2);
	  $it1=$it3;
	}
	else {
	  ($ic2,$ic3)=GetCloseTagContent(\$taglistlc,$tag,$it3);
	  $_=substr($taglist,$it2,$ic3-$it2);
	  $it1=$ic3;
	}
	if($tag eq "tag") {
	  #($content)=m/content=["'](.*)["']/;
	  #($tag)=m|>(.*)</tag>|;
	  ($tag)=m/key=["'](.*?)["']/si;
	  ($content)=m|>(.*)</tag>|si;
	  $tageditable=$editable;
	  $tags{$tag}=$content; # To allow automatic translation
	  if(length($content)==0) {
	    $tageditable=" nocontent style=\"background-color: gray; color: white;\"";
	    if($tag eq "front-page") { $content="HOME"; }
	    else { $content=$tag; }
	  }
	  $tagcomp=lc($tag);
	  if($tagcomp eq "front-page") {
	    $url=$homepage;
	  }
	  else {
	    $url="$fsfehost/tags/tagged-$tag.html";
	  }
	  $tags.="  <li key=\"$tag\">".AddID("<a href=\"$url\"$tageditable>")."$content</a></li>\n";
	  $tagsid{$tag}=$newid;
	  $tagsid{$tag}=~s/ id="$prefix//;
	  $tagsid{$tag}=~s/"//;
	}
      }
      #$_="</ul></aside>\n";
      #$_="</ul></aside></section></body>\n";
      #$_="</ul></main></body>\n";
      $tags.="</ul></footer>\n";
      $_="";
      if(!$xml) {
	#$_="</div></main><replacepod/></body>\n";
      }
    }
    elsif($tag eq "podcast")
    {
      s|<podcast(.*?)>|<div id="podcast$idsuffix" style="margin: 30px"$1><p><b>PODCAST METADATA:</b></p>|si;
      $podcast=$_;
      ($ic2,$ic3,$chapters)=GetCloseTagContent(\$htmllc,$tag,$i3,\$html);
      $i=$ic3;
      $it1=0;
      $it2=0;
      while($it2>=0 && defined($it1)) {
	($tag,$it2,$it3,$tagclosed)=GetNextTag(\$chapters,$it1);
	if($it2<0) {
	  last;
	}
	if($tagclosed || ($tag=~m/chapters|mp3|opus/)) {
	  $_=substr($chapters,$it2,$it3-$it2);
	  $it1=$it3;
	}
	else {
	  ($ic2,$ic3)=GetCloseTagContent(\$chapters,$tag,$it3);
	  $_=substr($chapters,$it2,$ic3-$it2);
	  $it1=$ic3;
	}
	if($tag eq "subtitle")
	{
	  NewID();
	  s|<subtitle>|<h5$newid$editable><em>|gis;
	  s|</subtitle>|</em></h5>|gis;
	}
	elsif($tag=~m|chapters|)
	{
	  s|chapters|ul|gis;
	}
	elsif($tag eq "chapter")
	{
	  ($start)=m/start=["'](.*?)["']/si;
	  NewID();
	  s|<chapter(.*?)>|<li$1><b>$start </b><span$newid$editable>|gsi;
	  s|</chapter>|</span></li>|gsi;
	}
	elsif($tag eq "url") {
	  ($url)=m|<url>(.*)</url>|si;
	  s|<url>|<p><a href="$url">|si;
	  s|</url>|</a> - |si;
	}
	elsif($tag eq "length") {
	  s|<length>|size=|si;
	  s|</length>|</p>|si;
	}
	elsif($tag eq "duration") {
	  s|<duration>|<p>Length: |sgi;
	  s|duration>|p>|sgi;
	}
	elsif($tag eq "type" || $tag eq "episode") {
	  $_="";
	}
	$podcast.="$_\n";
      }
      $podcast.="</div>\n";
      $_="";
    }
    elsif($tag eq "image")
    {
      ($image)=m/url=\s*"(\S+?)"/si;
      ($imagealt)=m/alt=\s*"(.*?)"/si;
      s/(alt=\s*)"(.*?)"/$1""/si;
    }
    if($issidebar) {
      $sidebar.=$_;
      $_="";
    }
    $buf.=$_;
  }

  $buf=~s|<replacetags/>|$tags|si;
  $buf=~s|<replaceside/>|$sidebar|si;
  $buf=~s|<replacepod/>|$podcast|si;
  $rp="";
  if(length($author)>0) {
    $rp="<span class='written-by'>Written by </span>\n".
	"<a class='author p-author h-card' rel='author' href='$fsfehost/about/people/$author/$author.en.html'>\n".
	"<img alt='' src='$fsfehost/about/people/avatars/$author.jpg'>$author</a>\n".
	"<span class='published-on'>on </span><time class='dt-published'>$newsdate</time>\n";
  }

  $buf=~s|<replaceauthor/>|$rp|si;
  if($xml) {
    $buf=~s|<head>|<head>$titlexml\n|si;
  }
  return($buf);
}

sub DiffXML {
  my($old,$new)=@_;
  Debug(3,"****** OLD ******\n",
          $old,"\n",
          "****** NEW ******\n",
          $new,"\n");
  $html=DiffHTML($old,$new);
  $isdiff=1;
  $contexthref=0;
  $buf=GenerateHTML($html,"webpreview");
  $isdiff=0;
  $buf=~s|.*(<main>)|$1|si;
  $buf=~s|(</main>).*|$1|si;
  Debug(3,"****** DIFF ******\n",
          $html,"\n",
          "****** BUF ******\n",
          $buf,"\n");
  return($buf);
}

sub FilenameFromURL {
  my $url=shift;
  my ($i);
  $i=rindex($url,"/");
  if($i>=0) { $filename=substr($url,$i+1); }
  else { $filename=$url; }
  if($filename=~m/^activity\.|^index\./) {
    # Get also dir name
    if($i>0) {
      $i=rindex($url,"/",$i-1);
    }
    if($i>=0) {
      $filename=substr($url,$i+1);
      $filename=~s/\//-/g;
    }
  }
  return($filename);
}

########### MAIN ##########
#$onlypreview=0; # TO-DO: make it customizable

if($db) { CloseDB($db); }

#$extra="IP=$ENV{REMOTE_ADDR}";
$extra="";

$padid="--";
$review="--";

$editable=" contenteditable=\"false\" onClick=\"ShowOrig(this)\" onFocus=\"ShowOrig(this)\"";
#if($onlypreview) { $editable=" contenteditable=\"false\""; }

%in=ReadParse(ReadParam(),'&');
%cookie=ReadParse($ENV{HTTP_COOKIE},'; ');
if(length($cookie{FORMAT})==0) { $cookie{FORMAT}="linebreak:break savesame:1"; }
%formatting=SplitCookie($cookie{FORMAT});
foreach(qw(linebreak savesame)) {
  if(length($in{$_})>0) { $formatting{$_}=$in{$_}; }
}
$in{lock}+=0;
$extra="linebreak=$formatting{linebreak}\tsavesame=$formatting{savesame}\tlock=$in{lock}\t";

#print STDERR "IN=".join(" # ",%in)."\n";

if($ENV{REQUEST_METHOD} ne "POST")
{
  # Limit only to "pad" and "rev" parameters for GET method
  %incopy=%in;
  %in=();
  foreach(qw(pad rev ro diff))
  {
    if(exists($incopy{$_})) { $in{$_}=$incopy{$_}; }
  }
}

$in{title}=TitleQuote($in{title});
$in{imagealt}=ValQuote(TitleQuote($in{imagealt}));

if($ENV{HTTP_HOST} eq "fsfe.org") {
  $fsfehost="";
}
else {
  $fsfehost=$fsfebase;
}
$imagebase=$fsfehost;

if(length($homepage=$in{homepage})==0) {
  $homepage=$ENV{HTTP_REFERER};
  $homepage=~s|.*?//.*?/|/|;
  $homepage=~s|\?.*||;
}
if($homepage=~m/cgi/ || length($homepage)==0) {
  $homepage=$defaulthome;
}
$homedir=$homepage;
$homedir=~s|(.*)/.*|$1|;

# Serve incoming XML request
if($in{cmd} eq "padid") {
  print "Content-Type: text/plain\n\n";
  # Send only random requests
  $db=ConnectDB($dbname);
  if($db) {
    sqldo($db,"BEGIN TRANSACTION");
    $c=sqlexe($db,"SELECT max(id) as maxid FROM pad");
    if(!defined($c)) {
      sqldo($db,"ROLLBACK");
      sqldo($db,"BEGIN TRANSACTION");
      # Database empty, create tables
      sqldo($db,"CREATE TABLE pad (id integer primary key, lastreview integer, filename varchar, title varchar,lastupdate datetimeinteger,extra varchar)");
      sqldo($db,"CREATE TABLE review (padid integer, review integer, origreview integer, filename varchar, title varchar, xml varchar,lastupdate datetimeinteger,extra varchar)");
      sqldo($db,"CREATE INDEX pad_filename ON pad (filename)");
      sqldo($db,"CREATE UNIQUE INDEX review_pk ON review (padid, review)");
      sqldo($db,"CREATE INDEX review_padid ON review (padid)");
      sqldo($db,"CREATE TABLE tags (tag varchar, lang varchar, orig varchar, translation varchar, lastupdate datetimeinteger)");
      sqldo($db,"CREATE UNIQUE INDEX tags_lang ON tags (tag, lang, orig)");
      sqldo($db,"COMMIT");
      sqldo($db,"BEGIN TRANSACTION");
      $c=sqlexe($db,"SELECT max(id) FROM pad");
    }
    if(!defined($c)) {
      $padid=1;
    }
    else {
      $r=$c->fetchrow_hashref;
      $c->finish;
      $padid=$r->{maxid}+1;
    }
    # Default review number
    $newreview=1;
    $refreview=0;
    $refpad=undef;
    $review=undef;
    $origreview=undef;
    $lastreview=undef;

    if($in{padid}>0) {
      # Search the selected PAD
      $c=sqlexe($db,"SELECT * FROM pad WHERE id=?",$in{padid});
      if(defined($c)) {
        $r=$c->fetchrow_hashref;
        $c->finish;
        $lastreview=$r->{lastreview};
	$padid=$r->{id};
      }
      if($in{review} ne "") {
        $c=sqlexe($db,"SELECT * FROM review WHERE padid=? AND review=?",$in{padid},$in{review});
	if(defined($c)) {
          $r=$c->fetchrow_hashref;
          $c->finish;
	  if(defined($r)) {
            $review=$r->{review};
	    $origreview=$r->{origreview};
	  }
	}
      }
    }
    if(substr($in{filename},0,6) eq "padxxx") {
      $in{filename}=~s/xxx/$padid/;
    }
    if(substr($in{origfilename},0,6) eq "padxxx") {
      $in{origfilename}=~s/xxx/$padid/;
    }
    $refpad=$origreview;
    if($in{translate}>0)
    {
      if(defined($review)) {
        $newreview=0;
      }
      elsif(defined($lastreview)) {
	$refreview=$lastreview;
        $review=$lastreview+1;
      }
      else {
	$newreview=3;
	$refreview=-1;
        $review=0;
      }
      if(length($in{refpad})>0) {
        $refpad=$in{refpad};
      }
      else {
        $refpad=$refreview;
      }
    }
    elsif($in{original}>0)
    {
      $newreview=2;
      $refpad=$refreview=0;
      $review=0;
    }
    elsif($in{newreview}>0)
    {
      if(defined($lastreview)) {
	$refreview=$lastreview;
        $review=$lastreview+1;
      }
      else {
	$newreview=3;
        $review=1;
      }
    }
    else #if($in{proofread}>0)
    {
      if(defined($review) && $review>=-99) {
        $newreview=0;
      }
      elsif(defined($lastreview)) {
	if($in{review} <= $lastreview) {
	  $refreview=$review=int($in{review});
	}
	else {
	  $refreview=$lastreview;
	  $review=$lastreview+1;
        }
      }
      else {
        $review=0;
      }
    }
    if($newreview>=2) {
      sqldo($db,"INSERT INTO pad (id,lastreview,filename,title,lastupdate,extra) VALUES (?,?,?,?,?,?)",$padid,$review,$in{filename},$in{title},time(),$extra);
      if($newreview>=3) {
        # Insert original review (0)
        sqldo($db,"INSERT INTO review (padid,review,origreview,filename,title,xml,lastupdate,extra) VALUES (?,?,?,?,?,?,?,?)",$padid,$refreview,$refpad,$in{origfilename},$in{title},$in{orightml},time(),$extra);
      }
    }
    if($newreview>0) {
      if($lastreview<$review) { $lastreview=$review; }
      else { $lastreview+=0; }
      sqldo($db,"INSERT INTO review (padid,review,origreview,filename,title,xml,lastupdate,extra) VALUES (?,?,?,?,?,?,?,?)",$padid,$review,$refpad,$in{filename},$in{title},RebuildXML($in{orightml},$in{newhtml}),time(),$extra);
    }
    else {
      sqldo($db,"UPDATE review SET filename=?,title=?,xml=?,lastupdate=?,extra=? WHERE padid=? AND review=?",$in{filename},$in{title},RebuildXML($in{orightml},$in{newhtml}),time(),$extra,$padid,$review);
    }
    if($lastreview<$review) {
      $lastreview=$review;
    }
    sqldo($db,"UPDATE pad SET lastreview=?,filename=?,title=?,lastupdate=? WHERE id=?",$lastreview,$in{filename},$in{title},time(),$padid);

    # Update tags translation memory
    %itags=();
    %utags=();
    $c=$db->prepare("SELECT * FROM tags WHERE tag=? AND lang=? AND orig=?");
    if(defined($c)) {
      foreach $key (keys %tags) {
	if(defined($tags{$key})) {
	  $c->execute($key,$in{lang},$origtags{$key});
	  $r=$c->fetchrow_hashref;
	  if(defined($r)) {
	    if($r->{translation} ne $newtags{$key}) {
	      $utags{$key}=$newtags{$key};
	    }
	  }
	  else {
	    $itags{$key}=$newtags{$key};
	  }
	}
      }
      $c->finish;
      $now=time();
      $c=$db->prepare("UPDATE tags SET translation=?, lastupdate=? WHERE tag=? AND lang=? AND orig=?");
      if(defined($c)) {
	foreach $key (keys %utags)
	{
	  $c->execute($newtags{$key},$now,$key,$in{lang},$origtags{$key});
	}
	$c->finish;
      }
      $c=$db->prepare("INSERT INTO tags (translation,lastupdate,tag,lang,orig) VALUES(?,?,?,?,?)");
      if(defined($c)) {
	foreach $key (keys %itags)
	{
	  $c->execute($newtags{$key},$now,$key,$in{lang},$origtags{$key});
	}
	$c->finish;
      }
    }
    sqldo($db,"COMMIT");
    CloseDB($db);
    $review="$review/$lastreview";
  }
  else {
    $padid="--";
    $review="--";
  }
  if($in{readonly}>0) { $in{cmd}.="-ro"; }
  print "$in{cmd}\n$padid\n$review";
  DebugClose();
  exit(0);
}

if($in{cmd} eq "transtags") {
  print "Content-Type: text/plain\n\n";
  # Send only random requests
  $db=ConnectDB($dbname);
  $totaltags=0;
  $transtags="";
  if($db) {
    %tags=();
    foreach(split("\t",$in{origtags}))
    {
      $totaltags++;
      ($tag,$value)=m/(\S+?)=(.*)/;
      $tags{$tag}=$value;
    }
    $c=$db->prepare("SELECT * FROM tags WHERE tag=? AND lang=? ORDER BY lastupdate");
    if(defined($c)) {
      foreach $tag (keys %tags)
      {
        $trans=$lasttrans=undef;
        $c->execute($tag,$in{lang});
	$count=0;
	while(defined($r=$c->fetchrow_hashref))
	{
	  $count++;
	  if($r->{orig} eq $tags{$tag}) { $trans=$r->{translation}; }
	  $lasttrans=$r->{translation};
	}
	if(!defined($trans) && $tags{$tag} eq "") { $trans=$lasttrans; }
	if(defined($trans)) {
	  $transtags.="$tag\t$tags{$tag}\t$trans\n";
	}
      }
      $c->finish;
    }
    CloseDB($db);
  }
  print "$in{cmd}\n$totaltags\n$transtags";
  DebugClose();
  exit(0);
}

if($in{cmd} eq "diff") {
  print "Content-Type: text/plain\n\n";
  # Send only random requests
  $db=ConnectDB($dbname);
  if($db) {
    if($in{padid}>0) {
      # Search the selected PAD
      $c=sqlexe($db,"SELECT * FROM review WHERE padid=? ORDER BY review DESC",$in{padid});
      if(defined($c)) {
	$oldrev=undef;
	$newrev=undef;
	while(defined($r=$c->fetchrow_hashref)) {
	  if(length($in{newrev})==0) { $in{newrev}=$r->{review}; }
	  if($r->{review}==$in{newrev}) {
	     $newrev=$r->{xml};
	  }
	  else {
	     if(length($in{oldrev})==0 && $r->{review}<$in{newrev}) { $in{oldrev}=$r->{review}; }
	     if($r->{review}==$in{oldrev}) {
	       $oldrev=$r->{xml};
	     }
	  }
	}
	$c->finish;
	if(length($oldrev)>0 && length($newrev)>0) {
	  $oldrev=~s|&#39;|'|gs;
	  $oldrev=~s|&quot;|"|gs;
	  $newrev=~s|&#39;|'|gs;
	  $newrev=~s|&quot;|"|gs;
	  $diffhtml=DiffXML($oldrev,$newrev);
	}
	else {
	  $diffhtml="OLDREV($in{oldrev}) or NEWREV($in{newrev}) not provided";
	}
      }
      else {
        $in{cmd}="fake";
      }
    }
  }
  print "$in{cmd}\n$diffhtml\n";
  DebugClose();
  exit(0);
}

# Maintenance functions
if($in{cmd} eq "exportdb") {
  $file="pad.db";
  print "Content-Type: application/octect-stream\n";
  print "Content-Disposition: attachment; filename=\"$file\"\n\n";
  if(open(F,$dbname)) {
     while(read(F,$buffer,1024*1024)) {
        print $buffer;
     }
     close(F);
  }
  DebugClose();
  exit(0);
}
# Create tag translation table and populate it
if($in{cmd} eq "createtag") {
  $|=1;
  print "Content-Type: text/plain\n\n";
  $db=ConnectDB($dbname);
  sqldo($db,"BEGIN TRANSACTION");
  sqldo($db,"CREATE TABLE tags (tag varchar, lang varchar, orig varchar, translation varchar, lastupdate datetimeinteger)");
  sqldo($db,"CREATE UNIQUE INDEX tags_lang ON tags (tag, lang, orig)");
  $c=sqlexe($db,"SELECT * FROM review WHERE review=-1"); # Select all translations
  $count=0;
  $ccount=0;
  if(defined($c)) {
    my (%itags);
    while(defined($r=$c->fetchrow_hashref)) {
      $c2=sqlexe($db,"SELECT * FROM review WHERE padid=? AND review>=0 ORDER BY review DESC LIMIT 1",$r->{padid});
      $r2=$c2->fetchrow_hashref;
      $c2->finish;
      $count++;
      if(defined($r2)) {
	($lang)=($r2->{filename}=~m/\.(..)\.xh?t?ml/);
	if(length($lang)>0 && $lang ne "en") {
	  GenerateHTML($r->{xml},"wptags");
	  %entags=%tags;
	  GenerateHTML($r2->{xml},"wptags");
	  foreach(keys %entags)
	  {
	    if($entags{$_} ne $tags{$_}) {
	      $key="$_\t$lang\t$entags{$_}";
	      if(defined($itags{$key})) {
	        ($t,$l)=split("\t",$itags{$key});
		if($l>$r2->{lastupdate}) { next; }
	      }
	      $itags{$key}="$tags{$_}\t$r2->{lastupdate}";
	    }
	  }
	}
      }
    }
    $c->finish;
    $c=$db->prepare("INSERT INTO tags (tag,lang,orig,translation,lastupdate) VALUES(?,?,?,?,?)");
    if(defined($c)) {
      foreach(keys %itags) {
	($tag,$lang,$orig)=split("\t");
	($trans,$lastupdate)=split("\t",$itags{$_});
	$ccount++;
	if(length($lastupdate)>0) { $lastupdate+=0; }
	else { $lastupdate=undef; }
	if($c->execute($tag,$lang,$orig,$trans,$lastupdate)) {
	  print "TAG $tag [$lang] $orig => $trans -- inserted.\n";
	}
	else {
	  print "Error inserting TAG $_:".$db->errstr." (".$db->err.")\n";
	}
      }
      $c->finish;
    }
    else {
	  print "Error preparing TAG insert:".$db->errstr." (".$db->err.")\n";
    }
  }
  print "ENDING creating tags, total created: $ccount (total pad: $count)\n";
  sqldo($db,"COMMIT");
  $db->disconnect();
  exit;
}

$reb=0;
$xml=0;
$exporting=0;
if(lc($in{savexml}) eq "yes") { $exporting=1; }
# Check if we have to revert back X[HT]ML
if(length($in{orightml})>0) {
  $buf=RebuildXML($in{orightml},$in{newhtml});
  if($exporting) {
    if($xml) { $file="export.xml"; }
    else { $file="export.xhtml"; }
    if($in{filename} ne "") {
      $file=$in{filename};
      $file=~s/\//-/g;
    }
    print "Content-Type: application/octect-stream\n";
    print "Content-Disposition: attachment; filename=\"$file\"\n\n";
    print $buf;
    DebugClose();
    exit(0);
  }

  # Rebuild the form
  print "Content-type: text/html\n\n";
  if(open(T,"template.html"))
  {
    while(<T>)
    {
      if(m/action.*fsfe\.pl/i) {
        s/fsfe\.pl/$script/;
      }
      elsif(m/var newhtml/) {
        $jshtml=JSQuote($buf);
        s/var newhtml="/var newhtml="$jshtml/;
      }
      elsif(m/var newpad/) {
        if($in{newpad} eq "--") { $in{newpad}=""; }
        $jshtml=JSQuote($in{newpad});
        s/var newpad="/var newpad="$jshtml/;
      }
      elsif(m/var newrev/) {
        if($in{newrev} eq "NaN") { $in{newrev}=""; }
        $jshtml=JSQuote($in{newrev});
        s/var newrev="/var newrev="$jshtml/;
      }
      elsif(m/var filename/) {
        s/var filename="/var filename="$in{filename}/;
      }
      print;
    }
    close(T);
  }
  DebugClose();
  exit(0);
}

print "Content-type: text/html\n\n";

$html="";
$oldhtml="";
$filename=$in{filename};
$origon=0;
$refpad="";

%extra=();

if(length($in{pad})>0) {
  # Get from DB
  $db=ConnectDB($dbname);
  if($db) {
    $padid=$in{pad};
    $padid=~s/^\s+|\s+$//g;
    if(!($padid=~m/^\d+$/)) {
      # Get PADID from filename
      if($padid=~m/^\^/) { $padid=substr($padid,1); }
      else { $padid="%".$padid; }
      if($padid=~m/\$$/) { $padid=substr($padid,0,-1); }
      else { $padid.="%"; }
      $padid=~s/\*/%/g;
      $c=sqlexe($db,"SELECT * FROM pad WHERE filename like ? ORDER BY id DESC LIMIT 1",$padid);
      if(defined($c)) {
        $r=$c->fetchrow_hashref;
	if(defined($r)) {
	  $padid=$r->{id};
	}
	else {
	  $padid="";
	}
      }
    }
    # Get last revision
    $c=sqlexe($db,"SELECT * FROM pad WHERE id=?",$padid);
    if(defined($c)) {
      $r=$c->fetchrow_hashref;
      $c->finish;
      if(defined($r)) {
        $lastreview=$r->{lastreview};
	$filename=$r->{filename};
        %extra=SplitExtra($r->{extra});
      }
      else {
	# PAD ID not found: give the error
	$c=sqlexe($db,"SELECT max(id) as maxid FROM pad");
	if(defined($c)) {
	  $r=$c->fetchrow_hashref;
	  $c->finish;
	  print "<!DOCTYPE html>\n";
	  print "<html><body>\n";
	  print "<H3><FONT COLOR=red>PAD not found: maximum ID $r->{maxid}</FONT></H3>\n";
	  print "<FORM><INPUT TYPE=button onClick=\"window.history.go(-1)\" VALUE=Back></FORM>\n";
	  print "</body></html>\n";
          DebugClose();
	  exit;
	}
      }
    }
    if($in{rev}=~m/del$/) {
      if($lastreview>0) {
        sqldo($db,"DELETE FROM review WHERE review=? AND padid=?",$lastreview,$padid);
        $lastreview--;
        sqldo($db,"UPDATE pad SET lastreview=? WHERE id=?",$lastreview,$padid);
      }
      $in{rev}="";
    }
    if($in{rev} eq "") {
      $review=$lastreview;
    }
    else {
      $review=$in{rev};
    }
    if($review>$lastreview || $review=~m/new/ || $in{new} eq "ON")
    {
      if($review eq "new" || $review>$lastreview || ($in{rev} eq "" && $in{new} eq "ON")) {
        $review=$lastreview+1;
      }
      else {
        $review=int($review);
      }
    }
    else
    {
      $c=sqlexe($db,"SELECT * FROM review WHERE padid=? AND review=?",$padid,$review);
      if(defined($c)) {
	$r=$c->fetchrow_hashref;
	$c->finish;
	if(defined($r)) {
	  $html=$r->{xml};
	  $filename=$r->{filename};
	  $refpad=$r->{origreview};
	  if(length($r->{extra})>0) {
            %extra=SplitExtra($r->{extra});
	  }
	}
      }
    }
    $oldhtml=$html;
    $r=undef;
    # Get orig html
    if($refpad=~m/#/) {
      ($rpad,$rrev)=split('#',$refpad);
      if(length($rrev)==0) {
	$c=sqlexe($db,"SELECT * FROM review WHERE padid=? ORDER BY review DESC LIMIT 1",$rpad);
      }
      else {
	$c=sqlexe($db,"SELECT * FROM review WHERE padid=? AND review=?",$rpad,$rrev);
      }
      if(defined($c)) {
	$r=$c->fetchrow_hashref;
	$c->finish;
	if(defined($r)) {
	  $origon=1;
	}
      }
    }

    if(!defined($r))
    {
      $c=sqlexe($db,"SELECT * FROM review WHERE padid=? AND review=?",$padid,-1);
      if(defined($c)) {
	$r=$c->fetchrow_hashref;
	$c->finish;
	if(!defined($r)) {
	  # Try with rev 0
	  $c=sqlexe($db,"SELECT * FROM review WHERE padid=? AND review=?",$padid,0);
	  if(defined($c)) {
	    $r=$c->fetchrow_hashref;
	    $c->finish;
	  }
	}
	else {
	  $origon=1;
	}
      }
    }
    if(defined($r)) {
      $oldhtml=$r->{xml};
    }
    CloseDB($db);
    foreach(qw(linebreak savesame)) {
      if(length($extra{$_})>0) { $formatting{$_}=$extra{$_}; }
    }
  }
  if(exists($in{ro}) && $in{ro} ne "0" && uc($in{ro}) ne "OFF") {
     $review=$padid="--";
     $lastreview="";
  }
  else {
    $review="$review/$lastreview";
  }
}

if(length($html)==0) {
  $html=$in{html};
  $html=~s/\r\n/\n/gs;
  $html=~s/\r/\n/gs;

  @html=split("\n",$html);

  if($#html<=2 && $html[0]=~m/^\s*[0-9]+\s*$/) {
    # A number on the first line: treat it as a pull request
    $html[0]=~s/^\s+|\s+$//g;
    $html[0]="$gitbaseweb/pulls/$html[0]";
  }


  # Check for link:
  $newhtml=undef;
  for($i=0;$i<=$#html;$i++)
  {
    $_=$html[$i];
    s/\s+//;
    if(length($_)>0)
    {
      if(m/^http/ || m|^/|)
      {
	if(m|^/|) { $_=$fsfebase.$_; }
	# Check if it is a pull request:
	if(m|$gitbaseweb/pulls|i)
	{
	  if(!m|/files|)
	  {
	     if(!m|/$|) { $_.="/"; }
	     $_.="files";
	  }
	}
	if(m|$gitbaseweb/|i && !m|xh?t?ml$|)
	{
	  # Get the branch and file list from the pull request
	  $url=$_;
	  #$_=`wget -O - "$url" 2>/dev/null`;
	  $merged=0;
	  $_=GetHTTP($url,$err);
	  if(length($_)>0)
	  {
	    if(m|Merged</div>|) { $merged=1; }
	    ($branch)=m|merge.*?commit.*?<code><a.*?>(\S+?)</a>.*?</code>|s;
	    if($branch=~m/:/) {
	      ($source,$branch)=split(":",$branch);
	    }
	    else {
	      $source="FSFE/fsfe-website";
	    }
	    @files=m|<span class="file.*?">(\S+?.x[ht]*ml)</span>|gs;
	    @src=m|<a.*?\shref=\s*\"(\S+.x[ht]*ml)\">View File</a>|gis;
	    # Filter-out any non-xhtml file (no more necessary due to regex above):
	    #for($k=0;$k<=$#files;$k++) {
	    #  if(!($files[$k]=~m/xhtml$/)) {
	    #    splice(@files,$k,1);
	    #    $k--;
	    #  }
	    #}
	    if(length($html[$i+1])>0)
	    {
	      $num=$html[$i+1];
	      if(0 && $num=~m/^\s*[0-9]+\s*$/)
	      {
		$num--;
		if($num<0) { $num=0; }
		$files[0]=$files[$num];
		$src[0]=$src[$num];
	      }
	      else
	      {
		# Search for a match
		@newfiles=();
		@newsrc=();
		for($k=0;$k<=$#files;$k++) {
		  if($files[$k]=~m/$num/) {
		    push(@newfiles,$files[$k]);
		    push(@newsrc,$src[$k]);
		    #$files[0]=$files[$k];
		    #$src[0]=$src[$k];
		    #last;
		  }
		}
		if($#newfiles>=0) {
		  @src=@newsrc;
		  @files=@newfiles;
		}
	      }
	    }
	    if($#files>0) {
	      # More than 1 source file: let the user choose it
	      if(open(T,"template.html"))
	      {
		while(<T>)
		{
		  if(m/var newhtml/) {
		    s/var newhtml="/var newhtml="$html[0]/;
		  }
		  if(m/<textarea/) {
		    # Insert the list
		    $buf="<b><font color=red>More than one file found, please select one:</font></b><br>\n";
		    $buf.="<select name=filelist SINGLE onClick=\"SelList(this)\" onChange=\"SelList(this)\">";
		    $buf.="<option value=\"\">&lt; Choose one &gt;</option>\n";
		    for($i=0;$i<=$#files;$i++) {
		      $buf.="<option value=\"$files[$i]\">$files[$i]</option>\n";
		    }
		    $buf.="</select><br>\n";
		    $_=$buf.$_;
		  }
		  print;
		}
		close(T);
	      }
              DebugClose();
	      exit(0);
	    }
	    if(!($files[0]=~m|^/|)) { $files[0]="/$files[0]"; }
	    if($src[0]=~m|^/|) { $src[0]=substr($src[0],1); }
	    if($source=~m|^/|) { $source=substr($source,1); }
	    if($merged) {
	      # Problems will arise if you try to get from an already merged branch: fetch directly from master branch
	      $baseurl="$gitbase/$source/raw/branch/master";
	      $imagebase=$baseurl;
	      $url="$baseurl$files[0]";
	    }
	    elsif($branch eq "") {
	      # Direct commit
	      sleep(3);
	      $url="$gitbase/$src[0]";
	      $url=~s|/src/|/raw/|;
	    }
	    else {
	      $baseurl="$gitbase/$source/raw/branch/$branch";
	      $url="$baseurl$files[0]";
	    }
	    $err="";
	    $newhtml=GetHTTP($url,$err);
	    if($newhtml=~m|<img src=\s*"/img/404.png" alt="404"/>|si || length($newhtml)==0 || $err=~m/404:? Not found/si) {
	      # File not found on the branch, try to download the source from pull request
	      if($branch eq "") {
	        sleep(5);
	      }
	      $url="$gitbase/$src[0]";
	      $url=~s|/src/|/raw/|;
	      $err="";
	      $retry=1;
	      while($retry>0)
	      {
		sleep(3);
		$err="";
		$newhtml=GetHTTP($url,$err);
		$err.=$newhtml;
		if($newhtml=~m|<img src=\s*"/img/404.png" alt="404"/>|si ||
		   $err=~m|503:? Service Temporarily Unavailable|si ||
		   length($newhtml)==0)
		{
		  $retry--;
		  if($err=~m/503:? Service Temporarily Unavailable|404:? Not Found/) {
		    if($retry>0) {
		      #select(undef,undef,undef,1+rand(2));
		      sleep(3);
		    }
		    else {
		      # As last resort, try from master branch
		      $brancherr=$url;
		      $newhtml="Service Temporrary Unavaiable for ${url}: retry later\n";
		      $baseurl="$gitbase/$source/raw/branch/master";
		      $imagebase=$baseurl;
		      $url="$baseurl$files[0]";
		      sleep(3);
		      $newhtml=GetHTTP($url,$err);
		      if($newhtml=~m|<img src=\s*"/img/404.png" alt="404"/>|si || length($newhtml)==0 || $err=~m/404:? Not found/si) {
			$newhtml="FILE $url NOT FOUND\n";
			$brancherr="";
		      }
		    }
		  }
		  else {
		    $newhtml="FILE $url NOT FOUND\n";
		    $retry=0;
		  }
		}
	      }
	    }
	    else {
	      $imagebase=$baseurl;
	    }
            $filename=FilenameFromURL($url);
	  }
	  else {
	    $newhtml="<b>ERROR fetching GIT page $url:</b> $err\n";
	  }
	}
	else # Not GIT pull request
	{
	  $newhtml="";
	  if(m/^$fsfebase/i) {
	    $url=$_;
	    $filename=FilenameFromURL($url);
	    $html2=GetHTTP($url,$err);
	    if($html2=~m|^<\?xml|) {
	      $newhtml=$html2;
	    }
	    elsif(($html2=~m/$gitbaseweb/si)) {
	      ($_)=($html2=~m/($gitbaseweb\S+?)"/si);
	    }
	  }
	  if(length($newhtml)==0) {
	    if(m/^$gitbase/) {
	      s|src/branch|raw/branch|;
	    }
	    $url=$_;
	    $filename=FilenameFromURL($url);
	    $newhtml=GetHTTP($url);
	  }
	}
      }
      else # Not and URL
      {
	$newhtml=undef;
      }
      last;
    }
  }
  if(length($newhtml)>0) {
    $html=$newhtml;
    $html=~s/\r\n/\n/gs;
    $html=~s/\r/\n/gs;
  }
  $oldhtml=$html;
}

#open(H,">cache/temp.temp");
#print H $html;
#close(H);


$jshtml=JSQuote($html);
$contexthref=0;
%tags=();
$bufold=GenerateHTML($oldhtml,"webpreview","orig");
$entags="";
foreach(keys %tags)
{
  $entags.="$_=$tags{$_}\t";
}
chop($entags);
$entags=JSQuote($entags);

$bufold=~s|.*(<main>)|<div id="oldcontent" style="display: none">\n|si;
if($bufold=~m|</main>.*<div id="podcast|) {
  $bufold=~s|</main>(<div id="podcast.*?</div>[\n\s]+).*|$1</div>\n|si;
}
else {
  $bufold=~s|</main>.*|</div>\n|si;
}
$oldimagealt=$imagealt;

$contexthref=1;
$buf=GenerateHTML($html,"webpreview");
## For debugging:
Debug(10,"### GENERATED ###\n", $buf, "### END ###\n");

$xmlerr="";
eval {
  local $SIG{ALRM} = sub { die "alarm\n" };
  my($wtr, $rdr, $err);
  $wtr=gensym;
  $rdr=gensym;
  $err=gensym;
  $pid = open3($wtr,$rdr,$err, "xmllint - ");
  print $wtr $html;
  alarm(5);
  close($wtr);
  while(<$err>)
  {
    $xmlerr.=$_;
  }
  waitpid( $pid, 0 );
  alarm(0);
  close($rdr);
  close($err);
};

$errortext="";

sub AddErr
{
  my($errstr)=@_;
  $errortext.=$errstr;
}

if(length($brancherr)>0)
{
  AddErr("<p><pre><font color=orange>Warning: could not fetch from $brancherr, using master branch</font></pre></p>\n");
}
if(length($buf)==0 || $body<2)
{
  AddErr("<H3><FONT COLOR=red>It seems you sent a not valid x[ht]ml file...</FONT></H3>\n");
}

if(length($xmlerr)>0) {
  AddErr("<p><pre><font color=red>".HTMLQuote($xmlerr)."</font></pre></p>\n");
}

$linkpad="$padid";
if(length($padid)>0 && $padid ne "--") {
  $linkpad="<a href=\"$homepage?pad=$padid\">$padid</a>";
}

$newform="\n<form name=\"form1\" action=$script method=post onSubmit=\"return(ComposeHTML(this))\">\n".
         "<input name=\"regenerate\" type=submit value=\"Regenerate X[HT]ML\">".
         "\&nbsp;<button name=\"exportxml\" class=\"editbtn\" type=button onClick=\"Export(this.form)\">Export X[HT]ML</button>".
	 "\&nbsp;<b>PAD: </b><span id=\"padid\">$linkpad</span>\n".
	 "\&nbsp;<b>Review: </b><span id=\"review\">$review</span>\n".
         "\&nbsp;<button id=\"readonly\" name=\"readonly\" class=\"btngray\" type=button onClick=\"ReadOnly(this)\">Reading</button>".
         "\&nbsp;<button id=\"proofread\" name=\"proofread\" class=\"editbtn\" type=button value=\"Proofread/Translate\" onClick=\"Proofread(this)\">Proofread/Translate</button>".
         "\&nbsp;<button id=\"origon\" name=\"origon\" class=\"btn".($origon?"green":"gray")."\" type=button onClick=\"OrigOnOff(this)\">Orig</button>".
         "\&nbsp;<button id=\"lock\" name=\"lock\" class=\"btn".($extra{lock}?"red":"green")."\" type=button onClick=\"Lock(1)\"><img id=\"imglock\" border=0 src=\"$homedir/".($extra{lock}?"":"un")."lock.png\" border=0></button>".
         "\&nbsp;<button id=\"revdiff\" name=\"revdiff\" class=\"editbtn\" type=button onClick=\"Diff(this)\">Review DIFF</button>".
         "\&nbsp;<span id=\"message\"></span>".

	 "<br><b>\&nbsp;X[HT]ML export formatting:\&nbsp;</b><select name=linebreak onChange=\"SetLineBreak(this)\">\n";
@formats=("none","Leave unchanged (same as browser output)",
          "break","Break lines in git-friendly way",
	  "join","Join section lines into a single line",
         );
for($i=0;$i<=$#formats;$i+=2) {
  $newform.="<option value=\"$formats[$i]\"".($formatting{linebreak} eq $formats[$i]?" selected":"").">".$formats[$i+1]."</option>\n";
}
$newform.="</select>\n&nbsp;<input type=hidden name=savesame value=\"$formatting{savesame}\"><label style=\"font-weight: normal;\"><input type=checkbox name=savesamechk onClick=\"SetLineBreak(this)\"".($formatting{savesame}?" CHECKED":"").">\&nbsp;Save the pad like export formatting (facilitate git-diff)</label>";

$newform.=
	 "<br>\&nbsp;<b>Ver:</b><input name=version value=\"$version\" size=1 onChange=\"Updated(this)\">\n".
	 "\&nbsp;<b>Date:</b><input ".(length($htmlnewsdate)==0?"disabled ":"")."name=htmlnewsdate value=\"$htmlnewsdate\" size=7 onChange=\"Updated(this)\">\n".
	 "\&nbsp;<b>Translator[s]: </b><input ".($xml?"disabled ":"")."name=translator value=\"$translator\" size=22 onChange=\"Updated(this)\">\n".
	 "\&nbsp;<b>Filename: </b><input name=filename value=\"$filename\" size=15 onChange=\"Updated(this)\">\n".
	 "\&nbsp;<b>Title: </b><span id=\"titlelen\">0/80</span>\n".
	 "\&nbsp;<b>Teaser: </b><span id=\"teaserlen\">0/350</span>\n".
         "<input type=hidden name=origfilename value=\"\">\n".
         "<input type=hidden name=orightml value=\"\">\n".
         "<input type=hidden name=origtags value=\"\">\n".
         "<input type=hidden name=newhtml value=\"\">\n".
         "<input type=hidden name=newpad value=\"\">\n".
         "<input type=hidden name=newrev value=\"\">\n".
         "<input type=hidden name=title value=\"\">\n".
         "<input type=hidden name=homepage value=\"$homepage\">\n".
         "<input type=hidden name=savexml value=\"\">\n".
	 "";
if($title_ok<=0 && !$xml) {
  $newform.="<br>\&nbsp;<b>TITLE:  </b><input name=title_noh1 value=\"".ValQuote($titlehtml)."\" size=80 onChange=\"Updated(this)\">\n";
}
if(defined($meta{description})) {
  $newform.="<br>\&nbsp;<b>Description: </b><input name=meta_description value=\"".ValQuote($meta{description})."\" size=80 onChange=\"Updated(this)\">\n";
}
if(defined($meta{keywords})) {
  $newform.="<br>\&nbsp;<b>Keywords:  </b><input name=meta_keywords value=\"".ValQuote($meta{keywords})."\" size=80 onChange=\"Updated(this)\">\n";
}
$footerimage="";
if(length($image)) {
  $newform.="<input type=hidden value=\"$image\" name=\"imageurl\">\n";
  $newform.="<input type=hidden value=\"$imagealt\" name=\"imagealt\">\n";
  $newform.="<input type=hidden value=\"$oldimagealt\" name=\"oldimagealt\">\n";
  $footerimage="<br><br>\&nbsp;\&nbsp;&nbsp;<img src=\"$image\" alt=\"$imagealt\" title=\"$imagealt\" style=\"width: 30%\" onClick=\"ChangeAlt(this,document.form1.imageurl,document.form1.imagealt,document.form1.oldimagealt)\"><br><br>\n";
}
$newform.="</form>\n";
#if($onlypreview) { $newform=""; }
if($xml eq "") { $xml=0; }
if($debug)
{
  $newform.="<script>\n<!--\n";

  if(open(JS,"send.js"))
  {
    while(<JS>) { $newform.=$_; }
    close(JS);
  }
  $newform.="\n//-->\n</script>\n";
}
else {
  @s=stat("send.js");
  $newform.="<script src=\"${defaulthome}send.js?ver=${VERSION}_$s[9]\"></script>\n";
}
$extra{lock}+=0;
$newform.=<<EOF;
<script>
<!--
var fsfescript="$script";
var homepage="$homepage";
var homedir="$homedir";
var maxlentitle=80;
var minlenteaser=200;
var maxlenteaser=350;
var orightml="$jshtml";
var padid="$padid";
var review=parseInt("$review",10);
var lastreview=parseInt("$lastreview",10);
var revedit=null;
var xml=$xml;
var origon=$origon;
var diffrev='$in{diff}';
var origtags="$entags";
var tagid=new Hash();
var lock=$extra{lock};

//-->
</script>
<script>
<!--
EOF

foreach(keys %tagsid) {
  $newform.= "tagid['$_']='$tagsid{$_}';\n";
}

$newform.="//-->\n</script>\n";

$newform.=<<EOF;
<br>
<div id="choose" style="display: none; position: fixed; top: 20%; left: 20%; margin: 0px; padding: 15px; background-color: #EFEFEF; min-height : 200px; height: 70%; width: 60%; overflow : auto; border : 1px solid #606060; box-shadow : 5px 5px 10px #888888; z-index: 99;"></div>
<div id="origtext" style="display: none; position: absolute; top: 0; font-size: 0.75em; left: 66%; margin: 0px; padding: 5px; background-color: #F8FFF8; width: 33%; overflow : auto; border : 1px solid #606060; box-shadow : 5px 5px 10px #888888; z-index: 99;"></div>
<div id="changeimg" style="display: none; position: absolute; top: 0; font-size: 0.75em; left: 5%; margin: 0px; padding: 5px; background-color: #E0E0E0; width: 60%; overflow : auto; border : 2px solid #6060F0; box-shadow : 5px 5px 10px #888888; z-index: 99;"></div>
<div id="allmain">
EOF
$disclaimer="";
if(open(F,"disclaimer.html")) {
  while(<F>) {
    $disclaimer.=$_;
  }
  close(F);
}

$buf=~s/<\/body>/<\/div>\n$footerimage\n$bufold<\/body>/si;

if($buf=~m/<body.*?>/) {
  $buf=~s/(<body.*?)>/$1 id="body">$disclaimer$errortext$newform/si;
}
elsif($buf=~m/<html.*?>/) {
  $buf=~s/(<html.*?>)/$1$disclaimer$errortext/si;
}
elsif($buf=~m/<section.*?>/) {
  $buf=~s/(<section.*?>)/$disclaimer$errortext$newform$1/si;
}
else {
  $buf.="$disclaimer$errortext";
}



print "<!DOCTYPE html>\n";
print $buf;

DebugClose();

#Test "db" dir permissions:
#print "<!--\n";
#print `ls -al`;
#print "DB:\n";
#print `ls -al db`;
#print "-->\n";

/*
SPDX-FileCopyrightText: 2020-2022 Luca Bonissi <lucabon@fsfe.org>

SPDX-License-Identifier: AGPL-3.0-or-later
*/

var cookie_days=60;
var message_timeout=5000;
var ses=null;
var ses_tid=null;
var lang="en";
var languages=new Array(
"ar","العربية","Arabic",
"eu","Euskara","Basque",
"bg","български","Bulgarian",
"bs","Bosanski","Bosnian",
"ca","Català","Catalan",
"cs","Český","Czech",
"da","Dansk","Danish",
"de","Deutsch","German",
"el","Ελληνικά","Greek",
"en","English","English",
"eo","Esperanto","Esperanto",
"es","Español","Spanish",
"et","Eesti","Estonian",
"fa","فارسی","Persian",
"fi","Suomi","Finnish",
"fr","Français","French",
"gl","Galego","Galician",
"he","עברית","Hebrew",
"hr","Hrvatski","Croatian",
"hu","Magyar","Hungarian",
"it","Italiano","Italian",
"ku","کوردی","Kurdish",
"lv","Latviešu valoda","Latvian",
"mk","Македонски","Macedonian",
"nb","Norsk (bokmål)","Norwegian (bokmål)",
"nl","Nederlands","Dutch",
"nn","Norsk (nynorsk)","Norwegian (nynorsk)",
"no","Norsk","Norwegian",
"pl","Polski","Polish",
"pt","Português","Portuguese",
"ro","Română","Romanian",
"ru","Русский","Russian",
"sk","Slovenčina","Slovak",
"sl","Slovenščina","Slovenian",
"sq","Shqip","Albanian",
"sr","Српски","Serbian",
"sv","Svenska","Swedish",
"tr","Türkçe","Turkish",
"uk","Українська","Ukrainian",
"zh","中文","Chinese"
);

function Hash()
{
  this.length=0;
  return this;
}

function SplitString(mystr,charsep)
{
  var i=0;
  var istart=0;
  this.length=0;
  if(mystr != null && mystr.length>0)
  {
    while(istart<=mystr.length)
    {
      if(istart<mystr.length)
	i=mystr.indexOf(charsep,istart);
      else i=-1;
      if(i==-1) i=mystr.length;
      this[this.length]=mystr.substring(istart,i);
      this.length++;
      istart=i+1;
    }
  }
  return(this);
}

function GetOneCookie(name)
{
  var mycookie="; "+document.cookie+";";
  var i=mycookie.indexOf("; "+name+"=",0);
  if(i>=0) {
    var iend=mycookie.indexOf(";",i+2);
    return mycookie.substring(i+3+name.length,iend);
  }
  return "";
}

function SetOneCookie(cookie_name,cookie_val,expire_days)
{
  var expire_date=null;
  if(expire_days!=null)
  {
    expire_date=new Date();
    expire_date.setTime(expire_date.getTime()+1000*60*60*24*expire_days);
  }
  var cookie_set=""+cookie_name+"="+cookie_val+"; ";
  if(expire_date!=null) cookie_set+="EXPIRES="+expire_date.toGMTString()+"; ";
  cookie_set+="PATH=/;";
  document.cookie=cookie_set;
}

function DeleteCookie(name)
{
  var edate=new Date();
  edate.setTime(edate.getTime()-1000*60*60*24*7);
  document.cookie=""+name+"=NULL; EXPIRES="+edate.toGMTString()+"; PATH=/;";
}

var mtid=null;

function Message(msg,timeout)
{
  var obj=document.getElementById("message");
  if(obj!=null) obj.innerHTML=msg;
  if(mtid!=null) clearTimeout(mtid);
  mtid=null;
  if(timeout!=null) mtid=setTimeout("Message('')",timeout);
}

function requesthandler()
{
  var res,i,cmd,obj,newpadid,trans,tag,count,tm;
  if(this.readyState == 4)
  {
    if(this.status == 200) // OK, read the response
    {
      res=this.responseText;
      if(ses_tid!=null) clearTimeout(ses_tid);
      ses_tid=null;
      // Handling result here:
      i=res.indexOf("\n");
      if(i>=0) {
        cmd=res.substring(0,i);
	res=res.substring(i+1,res.length);
      }
      else {
        cmd=res;
      }
      if(cmd.substring(0,5)=="padid") {
        i=res.indexOf("\n");
        newpadid=res.substring(0,i);
	review=res.substring(i+1,res.length);
	document.getElementById("review").innerHTML=review;
	i=review.indexOf("/");
	lastreview=parseInt(review.substring(i+1));
	review=parseInt(review.substring(0,i));
        if(cmd=="padid") revedit=review;
	if(padid.length==0 || padid=="--") {
	  Message("New PAD ID "+newpadid+" loaded.",message_timeout);
	  if(document.form1.filename.value.substring(0,6)=="padxxx") {
	    document.form1.filename.value="pad"+newpadid+document.form1.filename.value.substring(6);
	    //fields_updated++; // Filename just updated in perl script
	  }
	}
	else Message("PAD ID "+newpadid+"/"+review+" saved.",message_timeout);
	padid=newpadid;
	document.getElementById("padid").innerHTML="<a href=\""+homepage+"?pad="+padid+"\">"+padid+"</a>";
	if(nextoper=="transtags") TranslateTags(0);
      }
      else if(cmd.substring(0,4)=="diff") {
	viewdiff=1;
	document.getElementById("allmain").innerHTML=res;
	BgColor("revdiff","lgreen");
        //btn=document.form1.revdiff;
        //btn.style.backgroundColor="#80FF80";
        //btn.style.color="black";
      }
      else if(cmd.substring(0,9)=="transtags") {
        trans=new SplitString(res,"\n");
	total=trans[0];
	count=0;
	for(i=1;i<trans.length;i++)
	{
	  tag=new SplitString(trans[i],"\t");
	  if(tagid[tag[0]]!=null) {
	    obj=document.getElementById(tagid[tag[0]]);
	    if(obj!=null) {
	      if(obj.innerHTML=="" || obj.innerHTML==tag[1])
	      {
		count++;
	        obj.innerHTML=tag[2];
	      }
	    }
	  }
	}
	if(count>0) { updated=1; }
	if(nextoper!="transtags") tm=100;
	else tm=2000;
	setTimeout('Message("Auto-translated '+count+'/'+total+' tag[s]",'+message_timeout+')',tm);
        nextoper="";
      }
    }
    else if(this.status == 0) // Abort
    {
    }
  }
}

function SendRequest(cmd,extra)
{
  if(ses!=null) {
    ses.abort();
  }
  if(window.XMLHttpRequest)
  {
    ses = new XMLHttpRequest();
  }
  else {
    try {
      ses = new ActiveXObject("MSXML2.XMLHTTP.3.0");
    } catch(e) {
      try {
	ses = new ActiveXObject("Microsoft.XMLHTTP");
      } catch(e) {
	try {
	  ses = new ActiveXObject("Msxml2.XMLHTTP");
	} catch(e) {
	  ses = null;
	}
      }
    }
  }
  if(ses!=null)
  {
    ses.onreadystatechange = requesthandler;
    ses.open("POST",fsfescript);
    ses.send("cmd="+cmd
	     +"&homepage="+homepage
	     +"&xmlreq=1"
	     +"&"+extra
	     );
    return true;
  }
  else {
    //alert("Problem with session...");
    // Handle the request with a normal form/frame
    return false;
  }
}

function GetTitle()
{
  var obj,value="",frm;
  obj=document.getElementById("ttttt");
  if(obj!=null) value=obj.innerText;
  else {
    obj=document.form1["title_noh1"];
    if(obj!=null) value=obj.value;
  }
  return(value);
}

function TitleLen()
{
  var obj,len,value;
  value=GetTitle();
  len=value.length;
  obj=document.getElementById("titlelen");
  obj.innerHTML=""+len+"/"+maxlentitle;
  if(len>maxlentitle) obj.style.color="red";
  else obj.style.color="";
}
function TeaserLen()
{
  var obj,len;
  obj=document.getElementById("teaser");
  if(obj!=null) {
    len=obj.innerText.length;
    obj=document.getElementById("teaserlen");
    obj.innerHTML=""+len+"/"+maxlenteaser;
    if(len>maxlenteaser) obj.style.color="red";
    else if(len<minlenteaser) obj.style.color="orange";
    else obj.style.color="";
  }
}

function SetOnKeyUp()
{
  obj=document.getElementById("ttttt");
  if(obj==null) obj=document.form1["title_noh1"];
  if(obj!=null) {
    obj.onkeyup=TitleLen;
    TitleLen();
  }
  obj=document.getElementById("teaser");
  if(obj!=null) {
    obj.onkeyup=TeaserLen;
    TeaserLen();
  }
}

function LoadBody()
{
  var obj;
  SetOnKeyUp();
  window.onbeforeunload=UnloadBody;
  if(diffrev.length>0) {
    var i,oldrev="",newrev="";
    i=diffrev.indexOf("/");
    if(i>=0) {
      newrev=diffrev.substring(0,i);
      oldrev=diffrev.substring(i+1);
    }
    setTimeout('GoDIFF('+padid+',"'+oldrev+'","'+newrev+'")',1000);
  }
}

function UUU()
{
  var frm=document.form1;
  ComposeHTML(frm);
  if(lastsave_xml!=frm.newhtml.value || fields_updated) {
    if(confirm('Leave page without saving?')) return(true);
    SaveXML();
    return(false);
  }
  return(true);
}

function ComposeHTML(frm)
{
  RestoreOrig(document.form1.readonly);
  obj=document.getElementById("allmain");
  frm.orightml.value=orightml;
  frm.newhtml.value=obj.innerHTML;
  frm.title.value=GetTitle();
  frm.newpad.value=padid;
  frm.newrev.value=review;
  return(true);
}

function Export(frm) {
  ComposeHTML(frm);
  frm.savexml.value="yes";
  frm.submit();
  frm.savexml.value="";
}

function Editable(obj,value)
{
  if(obj.contentEditable=="false" || obj.contentEditable=="true") obj.contentEditable=value;
  else {
    // Scan all child objects
    obj=obj.firstElementChild;
    while(obj!=null) {
      Editable(obj,value);
      obj=obj.nextElementSibling;
    }
  }
}

function DefaultColor()
{
  document.form1.proofread.className="editbtn";
  document.form1.readonly.className="editbtn";
  document.form1.revdiff.className="editbtn";
  /*
  document.form1.proofread.style.backgroundColor=document.form1.regenerate.style.backgroundColor;
  //document.form1.translate.style.backgroundColor=document.form1.regenerate.style.backgroundColor;
  document.form1.readonly.style.backgroundColor=document.form1.regenerate.style.backgroundColor;
  //document.form1.newreview.style.backgroundColor=document.form1.regenerate.style.backgroundColor;
  document.form1.revdiff.style.backgroundColor=document.form1.regenerate.style.backgroundColor;
  document.form1.revdiff.style.color=document.form1.regenerate.style.color;
  */
}

var lastsave_xml="";
var fields_updated=0;
var save_counter=0;

function Updated()
{
  fields_updated++;
}

function ClearAutosaveID()
{
  if(autosave_id!=null) clearTimeout(autosave_id);
  autosave_id=null;
}

function SetAutosaveID()
{
  autosave_id=setTimeout("SaveXML()",55000+Math.floor(Math.random()*10000));
}

function UnloadBody()
{
  var frm=document.form1;

  if(padid.length==0 || padid=="--" || revedit==null) return;

  ComposeHTML(frm);
  if(lastsave_xml!=frm.newhtml.value || fields_updated) {
    //SaveXML();
    ClearAutosaveID();
    SetAutosaveID();
    return("Leave page without saving");
  }
  return;
}

function TranslateTags(closec)
{
  var s="";
  if(closec==null) closec=0;
  var frm=document.form1;
  LangFromFilename();
  if(lang!="") {
    //ComposeHTML(frm);
    if(closec) Message("Getting tag translations...");
    s="padid="+padid+"&review="+review+"&lang="+lang+
      "&origtags="+encodeURIComponent(origtags);
    SendRequest("transtags",s);
  }
  if(closec) CloseChoose();
}

var refpad="";

function SaveXML(extra)
{
  var i;
  var s="";
  var encfields=new Array("newhtml","title","filename","origfilename","homepage","version","translator","htmlnewsdate","meta_description","meta_keywords","imageurl","imagealt","linebreak","savesame","lock");
  if(extra==null) extra="";
  var frm=document.form1;
  if(viewdiff) return;
  ClearAutosaveID();
  ComposeHTML(frm);
  if(lastsave_xml!=frm.newhtml.value || fields_updated) {
    save_counter++;
    Message("Saving XML...("+save_counter+")");
    s="padid="+padid+"&review="+review+"&orightml="+encodeURIComponent(orightml)+
      "&origtags="+encodeURIComponent(origtags)+"&lang="+lang+"&refpad="+encodeURIComponent(refpad);
    for(i=0;i<encfields.length;i++) {
      if(frm[encfields[i]]!=null) {
        s+="&"+encfields[i]+"="+encodeURIComponent(frm[encfields[i]].value);
      }
    }
    s+=extra;
    SendRequest("padid",s);
    lastsave_xml=frm.newhtml.value;
    fields_updated=0;
  }
  SetAutosaveID();
}

function Show(id)
{
  var obj=document.getElementById(id);
  if(obj!=null) obj.style.display="";
}

function Hide(id)
{
  var obj=document.getElementById(id);
  if(obj!=null) obj.style.display="none";
}

function BgColor(id,color)
{
  var obj=document.getElementById(id);
  if(obj!=null) obj.className="btn"+color;
}

function Lock(save)
{
  var obj;
  obj=document.getElementById("imglock");
  if(lock) {
    lock=0;
    if(obj!=null) obj.src=homedir+"/unlock.png";
    BgColor("lock","green");
  }
  else {
    lock=1;
    if(obj!=null) obj.src=homedir+"/lock.png";
    BgColor("lock","red");
  }
  document.form1.lock.value=lock;
  if(save)
  {
    Updated();
    SaveXML("&readonly=1");
    ReadOnly(document.form1.readonly);
  }
}


var autosave_id=null;

function RestoreOrig(btn)
{
  if(viewdiff && origcontent!=null) {
    obj.innerHTML=origcontent;
    viewdiff=0;
    origcontent=null;
    if(btn!=null) {
      DefaultColor();
      btn.className="btngray";
    }
  }
}

function Edit(btn,extra)
{
  var obj;
  obj=document.getElementById("allmain");
  RestoreOrig();
  SetOnKeyUp();
  if(origon) {
    origon=1;
    BgColor("origon","green");
  }
  Editable(obj,"true");
  DefaultColor();
  btn.className="btnred";
  lastsave_xml="";
  SaveXML(extra);
}

function CloseChoose()
{
  var obj;
  Hide("choose");
}

function SetLang(sel)
{
  if(sel.selectedIndex>=0) 
    sel.form.lang.value=sel.options[sel.selectedIndex].value;
  else sel.form.lang.value="";
}

function Diff(btn)
{
  var translate=0;
  var obj;
  var s="";
  var i;
  obj=document.getElementById("choose");
  s+="<big><b>Reviews DIFF</b></big><br><br>";
  s+="<form name=\"revdiff\">";
  if(review==null) {
    review="";
    oldreview="";
  }
  else {
    oldreview=review-1;
  }
  s+="Old Revision: <input size=2 type=input name=\"oldrev\" value=\""+oldreview+"\">&nbsp;<br>";
  s+="New Revision: <input size=2 type=input name=\"newrev\" value=\""+review+"\">&nbsp;<br>";
  s+="<input type=button class=\"editbtn\" value=\"SHOW DIFF[s]\" onClick=\"ReviewDIFF(this.form)\">&nbsp;";
  s+="<br><br><input type=button value=\"Cancel\" onClick=\"CloseChoose()\">&nbsp;";
  s+="</form>\n";
  if(obj!=null) {
    obj.innerHTML=s;
    obj.style.display="";
  }
}

function TranslateButton(newpad)
{
  var s="";
  if(newpad==null) newpad=0;
  SetFilename();
  var lang=GetOneCookie("LANG");
  s+="<input type=button class=\"editbtn\" value=\"TRANSLATE\" onClick=\"Translate(this.form,"+newpad+")\">&nbsp;";
  s+="LangCode: <input size=2 type=input name=\"lang\" value=\""+lang+"\">&nbsp;";
  s+="<select single onClick=\"SetLang(this)\" onChange=\"SetLang(this)\">\n";
  if(lang=="") { lang="en"; }
  for(i=0;i<languages.length;i+=3) {
    s+="<option "+(lang==languages[i]?"SELECTED":"")+" value=\""+languages[i]+"\">"+languages[i+1]+" ("+languages[i+2]+")</option>\n";
  }
  s+="</select><br>";
  s+="<input type=checkbox CHECKED name=\"autotranstags\">";
  s+="<label for=autotranstags>Automatically translate tags from translation memory</label>";
  s+="<br><br>";
  return(s);
}

function NewReviewButton()
{
  var s="";
  s+="<input type=button class=\"editbtn\" value=\"Create NEW REVIEW\" onClick=\"NewReview()\">&nbsp;";
  s+="<br><br>";
  return(s);
}

function Proofread(btn)
{
  var translate=0;
  var obj;
  var s="";
  var i;
  refpad="";
  if(revedit==null || lock) {
    //s+="<div id=\"changesongsearch2\" style=\"background-color: #E0E0E0; border-bottom: 1px solid black; margin: -15px -15px 0px -15px; padding: 15px; position: sticky; top: -15px;\"><small><div id=\"close\"><a href=\"javascript:CloseCS()\">CHIUDI</a></div>\n";
    Hide("origtext");
    Hide("changeimg");
    obj=document.getElementById("choose");
    s+="<big><b>PROOFREAD/TRANSLATE</b></big><br><br>";
    s+="<form name=\"prooftransl\">";
    if(lock) {
      translate=1;
      LangFromFilename();
      if(lang=="" || lang=="en") {
        s+=TranslateButton(1);
      }
      s+=NewReviewButton();
    }
    else if(padid.length==0 || padid=="--") {
      // No PAD ID, create a new one
      translate=1;
      s+=TranslateButton(0);
      s+="<input type=button class=\"editbtn\" value=\"PROOFREAD\" onClick=\"NewReview()\">&nbsp;<BR><BR>";
      s+="<input type=button class=\"editbtn\" value=\"Make original content\" onClick=\"Original()\">&nbsp;<BR><BR>";
      if(obj==null) { // Fallback in case something is wrong with choose DIV
        if(confirm("Do you want to create a new TRANSLATION?")) Edit(btn,"&translate=1");
        else Edit(btn,"&newreview=1");
      }
    }
    else {
      //if(lastreview==0) {
      //  if(confirm("Do you want to create a new TRANSLATION?")) {
      //	  translate=1;
      //	  Edit(btn,"&translate=1");
      //  }
      //}
      if(!translate) {
	s+=NewReviewButton();
        s+="<input type=button class=\"editbtn\" value=\"EDIT current review\" onClick=\"Save()\">&nbsp;";
	LangFromFilename();
	if(lang!="" && lang!="en")
	{
	  s+="<br><br><br>";
	  s+="<input type=button class=\"editbtn\" value=\"Translate TAGS\" onClick=\"TranslateTags(1)\">&nbsp;";
	}
	else {
	  s+="<br><br><br>";
	  s+=TranslateButton(1);
	}
        if(obj==null) { // Fallback in case something is wrong with choose DIV
          if(review>lastreview || confirm("Do you want to create a new REVIEW?")) Edit(btn,"&newreview=1");
	  else Edit(btn,"&save=1");
        }
      }
    }
    s+="<br><br><input type=button value=\"Cancel\" onClick=\"CloseChoose()\">&nbsp;";
    s+="</form>\n";
    if(obj!=null) {
      obj.innerHTML=s;
      obj.style.display="";
    }
  }
  else {
    Edit(btn,"&save=1");
  }
}

function SetFilename()
{
  var fname,i;
  fname=document.form1.filename.value;
  if(fname=="") {
    if(lang=="") lang="en";
    fname="padxxx."+lang+(xml?".xml":".xhtml");
    document.form1.filename.value=fname;
  }
  document.form1.origfilename.value=fname;
}

function LangFromFilename()
{
  var fname,i;
  if(lang=="" || lang=="en")
  {
    fname=document.form1.filename.value;
    if(fname!="") {
      if((i=fname.indexOf(".xml"))>0) {
	fname=fname.substring(0,i);
      }
      if((i=fname.indexOf(".xhtml"))>0) {
	fname=fname.substring(0,i);
      }
      i=fname.lastIndexOf(".");
      if(i>=0) lang=fname.substring(i+1);
    }
  }
}


var origcontent=null;
var viewdiff=0;

function GoDIFF(padid,oldrev,newrev)
{
  var s="padid="+padid+"&oldrev="+oldrev+"&newrev="+newrev;
  if(!viewdiff || origcontent==null) {
    obj=document.getElementById("allmain");
    origcontent=obj.innerHTML;
  }
  DefaultColor();
  btn=document.form1.revdiff;
  btn.className="btnyellow";
  SendRequest("diff",s);
}

function ReviewDIFF(frm)
{
  var fname,i,obj;
  ReadOnly();
  lang=frm.lang.value;
  CloseChoose();
  setTimeout('GoDIFF('+padid+',"'+frm.oldrev.value+'","'+frm.newrev.value+'")',100);
}

var nextoper="";

function Translate(frm,newpad)
{
  var fname,i;
  //SetFilename();
  if(newpad==null) newpad=0;
  origon=1;
  lang=frm.lang.value;
  if(lang!="") {
    SetOneCookie("LANG",lang,cookie_days);
    // Fix filename
    fname=document.form1.filename.value;
    if(fname=="" || fname.substring(0,6)=="padxxx") fname="padxxx."+lang+(xml?".xml":".xhtml");
    else {
      if((i=fname.indexOf(".xml"))<2) i=fname.indexOf(".xhtml");
      if(i>=2) {
	if(i==2) fname=lang+fname.substring(i);
	else if(fname.substring(i-3,i-2)==".") fname=fname.substring(0,i-2)+lang+fname.substring(i);
	else fname=fname.substring(0,i)+"."+lang+fname.substring(i);
      }
    }
    document.form1.filename.value=fname;
  }
  CloseChoose();
  nextoper="";
  if(frm.autotranstags.checked) nextoper="transtags";
  refpad="";
  if(lock || newpad) { 
    Lock(0);
    if(padid.length>0 && padid!="--") {
      refpad=""+padid+"#";
      if(review<lastreview) refpad+=review;
    }
    padid="";
    review="";
  }
  Edit(document.form1.proofread,"&translate=1");
}

function GoProofread()
{
  DefaultColor();
  document.form1.proofread.className="btnred";
}

function NewReview(btn)
{
  //SetFilename();
  if(lock) Lock(0);
  Edit(document.form1.proofread,"&newreview=1");
  //setTimeout('GoProofread()',5000);
  CloseChoose();
}

function Original(btn)
{
  //SetFilename();
  refpad="";
  if(lock) Lock(0);
  Edit(document.form1.proofread,"&original=1");
  //setTimeout('GoProofread()',5000);
  CloseChoose();
}

function Save(btn)
{
  //SetFilename();
  Edit(document.form1.proofread,"&save=1");
  CloseChoose();
}

function ReadOnly(btn)
{
  Hide("origtext");
  Hide("changeimg");
  //if(origon) origon=-1;
  //lastelid=null;
  //BgColor("origon","gray");
  if(btn!=null) RestoreOrig();
  if(revedit!=null) {
    SaveXML("&readonly=1");
  }
  revedit=null;
  obj=document.getElementById("allmain");
  // Scan all document to update contenteditable property
  Editable(obj,"false");
  DefaultColor();
  if(btn!=null) btn.className="btngray";
}

function HTMLQuote(str)
{
  var i;
  while((i=str.indexOf("<"))>=0) {
    str=str.substring(0,i)+"&lt;"+str.substring(i+1);
  }
  while((i=str.indexOf(">"))>=0) {
    str=str.substring(0,i)+"&gt;"+str.substring(i+1);
  }
  return(str);
}

var imgedit=null;
var inpurl=null;
var inpalt=null;

function CloseImg()
{
  Hide("changeimg");
}

function ChangeImg()
{
  var frm=document.fimg;
  if(frm!=null && imgedit!=null)
  {
      var newalt=frm.imgalt.value;
      var newsrc=frm.imgsrc.value;
      imgedit.alt=newalt;
      imgedit.title=newalt;
      imgedit.src=newsrc;
      if(inpurl!=null) {
        if(inpurl.value!=newsrc) Updated();
        inpurl.value=newsrc;
      }
      if(inpalt!=null) {
        if(inpalt.value!=newalt) Updated();
        inpalt.value=newalt;
      }
  }
  CloseImg();
}

function UpdateImgPreview()
{
  var frm=document.fimg;
  var img=document.getElementById("imgpreview");
  if(frm!=null && img!=null)
  {
      var newsrc=frm.imgsrc.value;
      img.src=newsrc;
  }
}

function ChangeAlt(img,iurl,ialt,origalt)
{
  var newalt=null;
  var odiv=document.getElementById("changeimg");
  var s;
  if(img.id!=null && img.id.substring(0,10)=="webpreview") {
    ShowOrig(img,1);
  }
  else if(origalt!=null) {
    ShowOrig(img,2,origalt);
  }
  if(revedit!=null && odiv!=null) {
    imgedit=img;
    inpurl=iurl;
    inpalt=ialt;
    var bdy=document.getElementById("body");
    var rect=img.getBoundingClientRect();
    var top=rect.top+window.scrollY+16;
    odiv.style.top=""+top+"px";
    odiv.innerHTML=
      "<form name=fimg>\n" +
      "Change IMG alt:<br>\n" +
      "<textarea name=imgalt style=\"width: 74%;\" rows=5>"+HTMLQuote(img.alt)+"</textarea>"+
      "<img id=\"imgpreview\" style=\"max-width: 25%; float: right;\" src=\""+img.src+"\"><br>\n"+
      "<br>\n"+
      "Change IMG URL:<br>\n" +
      "<input type=text name=imgsrc style=\"width: 100%\" value=\""+img.src+"\" onChange=\"UpdateImgPreview()\"><br>\n"+
      "<br>\n"+
      "<input type=button class=\"editbtn\" value=\"CHANGE\" onClick=\"ChangeImg()\">&nbsp;&nbsp;"+
      "<input type=button class=\"editbtn\" value=\"Cancel\" onClick=\"CloseImg()\">";
    odiv.style.display="";
    if(newalt!=null) {
    }
  }
}


function ChangeSrc(img,inp) // Used only for footer image
{
  var newalt=null;
  if(revedit!=null) {
    newalt=prompt("Change IMG URL:",img.src);
    if(newalt!=null) {
      img.src=newalt;
      if(inp!=null) {
        Updated();
        inp.value=newalt;
      }
    }
    return false;
  }
  return true;
}

function ChangeHref(anchor) 
{
  var newalt=null;
  if(revedit!=null) {
    newalt=prompt("Change URL:",anchor.href);
    if(newalt!=null) {
      anchor.href=newalt;
    }
    return false;
  }
  return true;
}

var lastelid=null;

function GetWidth(obj)
{
  var w=obj.width;
  if(w==null) w=obj.clientWidth;
  if(w==null) w=obj.offsetWidth;
  var rect=obj.getBoundingClientRect();
  w=rect.width;
  return(w);
}

function ShowOrig(obj,outer,inp)
{
  var oobj,odiv;
  var id=obj.id;
  if(origon<=0) return;
  //var cdiv=document.getElementById("content");
  if(id!=null && id.length>0) {
    oobj=document.getElementById(id+"orig");
    lastelid=id;
  }
  else if(inp!=null) {
    oobj=inp;
    lastelid=null;
  }
  if(oobj!=null) {
      odiv=document.getElementById("origtext");
      var s;
      if(outer==1) s=oobj.outerHTML;
      else if(outer==2) s=oobj.value;
      else s=oobj.innerHTML;
      var sl=s.toLowerCase();
      var i,i2;
      // Remove any image and leave only the ALT value
      while((i=sl.indexOf("<img"))>=0) {
        i2=sl.indexOf(">",i);
	if(i2>0) {
	  var img=sl.substring(i,i2);
	  var i3,i4;
	  if((i3=img.indexOf("alt="))>=0) {
	    // Check the quote character
	    i4=img.indexOf("\"",i3);
	    i3=img.indexOf("'",i3);
	    if(i3<0 || i3>i4) {
	      i3=i4+1;
	      i4=img.indexOf("\"",i3);
	    }
	    else {
	      i3++;
	      i4=img.indexOf("'",i3);
	    }
	    img=s.substring(i,i2);
	    if(i4>0) img=img.substring(i3,i4);
	    else img=img.substring(i3);
	    img="<span style=\"background-color: #FFFFB0;\">"+img+"</span>";
	  }
	  else img="";
	  s=s.substring(0,i)+img+s.substring(i2+1);
	  sl=sl.substring(0,i)+img+s.substring(i2+1);
	}
      }
      // Remove class in any div
      i=0;
      while((i=sl.indexOf("<div",i))>=0) {
        i2=sl.indexOf(">",i);
	if(i2>0) {
	  var el=sl.substring(i,i2+1);
	  var i3,i4,i5;
	  if((i3=el.indexOf("class="))>=0) {
	    // Check the quote character
	    i4=el.indexOf("\"",i3);
	    i5=el.indexOf("'",i3);
	    if(i5<0 || i5>i4) {
	      i5=i4+1;
	      i4=el.indexOf("\"",i5);
	    }
	    else {
	      i5++;
	      i4=el.indexOf("'",i5);
	    }
	    el=s.substring(i,i2+1);
	    if(i4>0) el=el.substring(0,i3)+el.substring(i4+1);
	  }
	  s=s.substring(0,i)+el+s.substring(i2+1);
	  sl=sl.substring(0,i)+el+sl.substring(i2+1);
          i++;
	}
        else i=sl.length;
      }
      var bdy=document.getElementById("body");
      var rect=obj.getBoundingClientRect();
      var top=rect.top+window.scrollY;
      var left;
      if(bdy==null || bdy.className.indexOf("news")>=0) {
        //top+=cdiv.offsetTop;
	if(rect.left>(window.innerWidth*0.4) && (rect.left+rect.width)>(window.innerWidth*0.66)) {
	  if(rect.left>(window.innerWidth*0.6)) left="30%";
	  else left="2%";
	}
	else left="66%";
      }
      else {
	var h;
        top+=rect.height;
	left=rect.left+window.scrollX+30;
	if((left+GetWidth(odiv))>(window.innerWidth-15)) left=window.innerWidth-GetWidth(odiv)-15;
	left=""+left+"px";
      }
      odiv.style.top=""+top+"px";
      odiv.style.left=left;
      odiv.innerHTML=s;
      odiv.style.display="";
  }
}

function OrigOnOff(btn)
{
  //if(origon>=0 && revedit!=null)
  if(origon>=0)
  {
    if(origon==0) {
      origon=1;
      if(lastelid!=null) {
         var obj=document.getElementById(lastelid);
	 if(obj!=null) ShowOrig(obj);
      }
      BgColor("origon","green");
    }
    else {
      origon=0;
      Hide("origtext");
      //lastelid=null;
      BgColor("origon","gray");
    }
  }
}

function SetLineBreak(inp)
{
  var frm=inp.form;
  var val;
  var sel=frm["linebreak"];
  var format="linebreak:";
  if(sel.selectedIndex>=0) format+=sel.options[sel.selectedIndex].value;
  else format+="none";
  var savesame="savesame:";
  sel=frm["savesamechk"]
  if(sel.checked) val="1";
  else val="0";
  format+=" savesame:"+val;
  frm.savesame.value=val;
  SetOneCookie("FORMAT",format,cookie_days);
}

function SetFormat()
{
  var format=GetOneCookie("FORMAT");
  if(format.length>0) SetOneCookie("FORMAT",format,cookie_days);
}

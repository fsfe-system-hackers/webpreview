# SPDX-FileCopyrightText: 2020 Free Software Foundation Europe <https://fsfe.org>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

FROM bitnami/apache:2.4

# Install dependencies
USER 0
RUN install_packages libxml2-utils libdbd-sqlite3-perl wget

USER 1001

# Copy CGI and HTML files
COPY www/ /app/
# Copy additional files
COPY www/index.html /app/cgi/template.html

# Enable required modules
RUN sed -i -r -e "s/#LoadModule cgid_module/LoadModule cgid_module/" \
        /opt/bitnami/apache/conf/httpd.conf

# Add custom config for Apache
COPY webpreview.conf /vhosts/

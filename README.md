<!--
SPDX-FileCopyrightText: 2020 Free Software Foundation Europe <https://fsfe.org>

SPDX-License-Identifier: AGPL-3.0-or-later
-->

[![in docs.fsfe.org](https://img.shields.io/badge/in%20docs.fsfe.org-OK-green)](https://docs.fsfe.org/repodocs/webpreview/00_README)

# XHTML file preview

This is a service initiated by the FSFE's translator Luca Bonissi to
preview XHTML files. These files are the foundation for pages on
fsfe.org.

Because the markup is often hard to imagine from the raw file, this
service offers a preview and a WYSIWYG editor. It uses the FSFE's design
and tried to emulate how the file will look like in the end.

It also checks for XML syntax errors. If they exist, they will be
displayed in red colour on the previewed page.


## Limitations

Certain page-specific layout and XSL rules cannot be emulated. However,
for a rough preview it suffices.


## Local tests

The services is deployed via Docker, so you can run:

`docker-compose up -d --build`

Then, identify the IP of your container and open it.


## License

This code is [REUSE compliant](https://reuse.software), so all
copyright and licensing information is stored within the files
themselves, or can be extracted with the REUSE helper tool.
